package com.nimbusds.infinispan.persistence.ldap.backend;


import com.nimbusds.common.monitor.MonitorRegistries;
import junit.framework.TestCase;


public class LDAPTimersTest extends TestCase {
	
	
	public void testInit() {
		
		LDAPTimers ldapTimers = new LDAPTimers("myCache.");
		
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get("myCache.ldapStore.getTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get("myCache.ldapStore.searchTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get("myCache.ldapStore.addTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get("myCache.ldapStore.modifyTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get("myCache.ldapStore.deleteTimer").getCount());
		
		assertEquals(0L, ldapTimers.getTimer.getCount());
		assertEquals(0L, ldapTimers.searchTimer.getCount());
		assertEquals(0L, ldapTimers.addTimer.getCount());
		assertEquals(0L, ldapTimers.modifyTimer.getCount());
		assertEquals(0L, ldapTimers.deleteTimer.getCount());
	}
	
	
	
	public void testTime() {
		
		LDAPTimers ldapTimers = new LDAPTimers("myCache.");
		
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get("myCache.ldapStore.getTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get("myCache.ldapStore.searchTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get("myCache.ldapStore.addTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get("myCache.ldapStore.modifyTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get("myCache.ldapStore.deleteTimer").getCount());
		
		assertEquals(0L, ldapTimers.getTimer.getCount());
		assertEquals(0L, ldapTimers.searchTimer.getCount());
		assertEquals(0L, ldapTimers.addTimer.getCount());
		assertEquals(0L, ldapTimers.modifyTimer.getCount());
		assertEquals(0L, ldapTimers.deleteTimer.getCount());
		
		ldapTimers.getTimer.time().stop();
		ldapTimers.searchTimer.time().stop();
		ldapTimers.addTimer.time().stop();
		ldapTimers.modifyTimer.time().stop();
		ldapTimers.deleteTimer.time().stop();
		
		assertEquals(1L, MonitorRegistries.getMetricRegistry().getTimers().get("myCache.ldapStore.getTimer").getCount());
		assertEquals(1L, MonitorRegistries.getMetricRegistry().getTimers().get("myCache.ldapStore.searchTimer").getCount());
		assertEquals(1L, MonitorRegistries.getMetricRegistry().getTimers().get("myCache.ldapStore.addTimer").getCount());
		assertEquals(1L, MonitorRegistries.getMetricRegistry().getTimers().get("myCache.ldapStore.modifyTimer").getCount());
		assertEquals(1L, MonitorRegistries.getMetricRegistry().getTimers().get("myCache.ldapStore.deleteTimer").getCount());
		
		assertEquals(1L, ldapTimers.getTimer.getCount());
		assertEquals(1L, ldapTimers.searchTimer.getCount());
		assertEquals(1L, ldapTimers.addTimer.getCount());
		assertEquals(1L, ldapTimers.modifyTimer.getCount());
		assertEquals(1L, ldapTimers.deleteTimer.getCount());
	}
}
