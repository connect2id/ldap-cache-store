package com.nimbusds.infinispan.persistence.ldap;


import junit.framework.TestCase;


public class LDAPWriteStrategyTest extends TestCase {
	

	public void testConstants() {

		assertEquals("TRY_LDAP_ADD_FIRST", LDAPWriteStrategy.TRY_LDAP_ADD_FIRST.name());
		assertEquals("TRY_LDAP_MODIFY_FIRST", LDAPWriteStrategy.TRY_LDAP_MODIFY_FIRST.name());

		assertEquals(2, LDAPWriteStrategy.values().length);
	}


	public void testGetDefault() {

		assertEquals(LDAPWriteStrategy.TRY_LDAP_ADD_FIRST, LDAPWriteStrategy.getDefault());
	}
}
