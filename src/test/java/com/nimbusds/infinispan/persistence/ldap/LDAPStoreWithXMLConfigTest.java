package com.nimbusds.infinispan.persistence.ldap;


import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import com.unboundid.ldap.sdk.LDAPException;
import com.unboundid.ldap.sdk.ReadOnlyEntry;
import org.infinispan.Cache;
import org.infinispan.context.Flag;
import org.infinispan.eviction.EvictionType;
import org.infinispan.manager.DefaultCacheManager;
import org.infinispan.manager.EmbeddedCacheManager;
import org.infinispan.persistence.spi.PersistenceException;


/**
 * Tests the LDAP store with an XML-based config.
 */
public class LDAPStoreWithXMLConfigTest extends TestWithLDAPServer {


	private static final String CACHE_NAME = "myMap";


	/**
	 * The Infinispan cache manager.
	 */
	protected EmbeddedCacheManager cacheMgr;


	@Override
	public void setUp()
		throws Exception {

		super.setUp();

		cacheMgr = new DefaultCacheManager("test-infinispan.xml");

		cacheMgr.start();
	}


	@Override
	public void tearDown()
		throws Exception {

		super.tearDown();

		if (cacheMgr != null) cacheMgr.stop();
	}


	public void testSimpleObjectLifeCycle()
		throws Exception {

		Cache<String,User> myMap = cacheMgr.getCache(CACHE_NAME);

		// Get non-existing object
		assertNull(myMap.get("invalid-key"));

		// Store new object
		String k1 = "alice";
		User u1 = new User("Alice Adams", "alice@wonderland.net");

		assertNull(myMap.putIfAbsent(k1, u1));

		// Check presence
		assertTrue(myMap.containsKey(k1));

		// Confirm LDAP write
		ReadOnlyEntry u1Entry = getTestLDAPServer().getEntry("uid="+k1+",ou=people,dc=wonderland,dc=net");
		assertEquals("Alice Adams",  u1Entry.getAttributeValue("cn"));
		assertEquals(1, u1Entry.getAttribute("cn").size());
		assertEquals("Adams",  u1Entry.getAttributeValue("sn"));
		assertEquals(1, u1Entry.getAttribute("sn").size());
		assertEquals("alice@wonderland.net",  u1Entry.getAttributeValue("mail"));
		assertEquals(1, u1Entry.getAttribute("mail").size());

		// Get object
		assertEquals("Alice Adams", myMap.get(k1).getName());
		assertEquals("alice@wonderland.net", myMap.get(k1).getEmail());

		// Update object
		u1 = new User("Bob Brown", "bob@wonderland.net");
		assertNotNull(myMap.replace(k1, u1));

		// Confirm LDAP modify
		u1Entry = getTestLDAPServer().getEntry("uid="+k1+",ou=people,dc=wonderland,dc=net");
		assertEquals("Bob Brown",  u1Entry.getAttributeValue("cn"));
		assertEquals(1, u1Entry.getAttribute("cn").size());
		assertEquals("Brown",  u1Entry.getAttributeValue("sn"));
		assertEquals(1, u1Entry.getAttribute("sn").size());
		assertEquals("bob@wonderland.net",  u1Entry.getAttributeValue("mail"));
		assertEquals(1, u1Entry.getAttribute("mail").size());

		// Get object
		assertEquals("Bob Brown", myMap.get(k1).getName());
		assertEquals("bob@wonderland.net", myMap.get(k1).getEmail());

		// Remove object
		u1 = myMap.remove(k1);
		assertEquals("Bob Brown", u1.getName());
		assertEquals("bob@wonderland.net", u1.getEmail());

		// Confirm removal
		assertNull(myMap.get(k1));
		assertFalse(myMap.containsKey(k1));

		// Confirm LDAP delete
		assertNull(getTestLDAPServer().getEntry("uid="+k1+",ou=people,dc=wonderland,dc=net"));
	}


	public void testMultiplePutIfAbsentWithEviction()
		throws Exception {

		Cache<String, User> myMap = cacheMgr.getCache(CACHE_NAME);
		
		assertEquals(EvictionType.COUNT, myMap.getCacheConfiguration().memory().evictionType());
		assertEquals(100, myMap.getCacheConfiguration().memory().size());

		String[] keys = new String[200];

		for (int i=0; i < 200; i ++) {

			String key = "a" + i;
			keys[i] = key;
			User value = new User("Alice Adams-" + i, "alice-" + i + "@email.com");

			assertNull(myMap.putIfAbsent(key, value));
		}

		// Confirm LDAP write
		for (int i=0; i < 200; i++) {

			ReadOnlyEntry entry = getTestLDAPServer().getEntry("uid="+keys[i]+",ou=people,dc=wonderland,dc=net");
			assertEquals("Alice Adams-" + i,  entry.getAttributeValue("cn"));
			assertEquals(1, entry.getAttribute("cn").size());
			assertEquals("Adams-" + i,  entry.getAttributeValue("sn"));
			assertEquals(1, entry.getAttribute("sn").size());
			assertEquals("alice-"+i+"@email.com",  entry.getAttributeValue("mail"));
			assertEquals(1, entry.getAttribute("mail").size());
		}

		// Calls AdvancedCacheLoader.process, result matches eviction size
		assertEquals(100, myMap.getAdvancedCache().getDataContainer().size());

		// Calls AdvancedCacheLoader.process, result equals total persisted
		assertEquals(200, myMap.size());

		// Check presence
		for (int i=0; i < 200; i ++) {
			assertTrue(myMap.containsKey(keys[i]));
		}

		assertEquals(200, myMap.size());

		// Evict entries from memory, LDAP store unaffected
		for (String uuid: keys) {
			myMap.evict(uuid);
		}

		assertEquals(200, myMap.size());

		// Recheck presence, causes entry to be loaded from the LDAP
		// store into memory
		for (int i=0; i < 200; i ++) {
			assertTrue(myMap.containsKey(keys[i]));
		}

		// Confirm LDAP presence
		for (int i=0; i < 200; i ++) {

			ReadOnlyEntry entry = getTestLDAPServer().getEntry("uid="+keys[i]+",ou=people,dc=wonderland,dc=net");
			assertEquals("Alice Adams-" + i,  entry.getAttributeValue("cn"));
			assertEquals(1, entry.getAttribute("cn").size());
			assertEquals("Adams-" + i,  entry.getAttributeValue("sn"));
			assertEquals(1, entry.getAttribute("sn").size());
			assertEquals("alice-"+i+"@email.com",  entry.getAttributeValue("mail"));
			assertEquals(1, entry.getAttribute("mail").size());
		}

		// Remove from cache and LDAP store
		for (int i=0; i < 200; i ++) {
			assertNotNull(myMap.remove(keys[i]));
		}

		assertEquals(0, myMap.size());

		// Confirm LDAP deletion
		for (int i=0; i < 200; i ++) {

			assertNull(getTestLDAPServer().getEntry("uid="+keys[i]+",ou=people,dc=wonderland,dc=net"));
		}

		// Confirm deletion
		for (int i=0; i < 200; i ++) {

			assertNull(myMap.get(keys[0]));
		}
	}


	public void testPutWithLDAPServerDown() {

		Cache<String,User> myMap = cacheMgr.getCache(CACHE_NAME);

		getTestLDAPServer().shutDown(true);

		String key = "alice";

		try {
			myMap.put(key, new User("Alice Adams", "alice@name.com"));
			fail();
		} catch (PersistenceException e) {
			assertTrue(e.getMessage().startsWith("LDAP get of uid="+key+",ou=people,dc=wonderland,dc=net failed:"));
			LDAPException ldapException = (LDAPException)e.getCause();
			int code = ldapException.getResultCode().intValue();
			assertTrue(code == 81 || code == 91);
		}
	}


	public void testPutAsyncWithLDAPServerDown()
		throws Exception {

		Cache<String,User> myMap = cacheMgr.getCache(CACHE_NAME);

		getTestLDAPServer().shutDown(true);

		String key = "alice";

		// No exception thrown here, instead in future
		CompletableFuture<User> future = myMap.putAsync(key, new User("Alice Adams", "alice@name.com"));

		while (! future.isDone()) {
			TimeUnit.MILLISECONDS.sleep(10);
		}

		try {
			future.get();
			fail();
		} catch (ExecutionException e) {
			assertTrue(e.getMessage().startsWith("org.infinispan.persistence.spi.PersistenceException: LDAP get of uid="+key+",ou=people,dc=wonderland,dc=net failed:"));
			PersistenceException persistenceException = (PersistenceException)e.getCause();
			LDAPException ldapException = (LDAPException)persistenceException.getCause();
			int code = ldapException.getResultCode().intValue();
			assertTrue(code == 81 || code == 91);
		}

		assertEquals(0, myMap.getAdvancedCache().withFlags(Flag.SKIP_CACHE_LOAD).size());
	}
}