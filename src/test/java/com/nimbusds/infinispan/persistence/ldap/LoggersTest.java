package com.nimbusds.infinispan.persistence.ldap;


import junit.framework.TestCase;


public class LoggersTest extends TestCase{
	

	public void testNames() {

		assertEquals("MAIN", Loggers.MAIN_LOG.getName());
		assertEquals("LDAP", Loggers.LDAP_LOG.getName());
	}
}
