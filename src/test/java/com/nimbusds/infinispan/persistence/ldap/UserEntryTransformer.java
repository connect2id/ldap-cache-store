package com.nimbusds.infinispan.persistence.ldap;


import java.util.*;

import com.nimbusds.infinispan.persistence.common.InfinispanEntry;
import com.nimbusds.infinispan.persistence.common.InternalMetadataBuilder;
import com.unboundid.ldap.sdk.*;
import org.infinispan.persistence.spi.PersistenceException;


/**
 * Transforms User POJO to / from LDAP directory entry.
 */
public class UserEntryTransformer implements LDAPEntryTransformer<String,User> {


	private static LDAPWriteStrategy writeStrategy = null;


	public static LDAPWriteStrategy getWriteStrategy() {
		return writeStrategy;
	}


	public static void setWriteStrategy(final LDAPWriteStrategy writeStrategy) {
		UserEntryTransformer.writeStrategy = writeStrategy;
	}


	@Override
	public Set<String> getModifiableAttributes() {
		return Collections.unmodifiableSet(new HashSet<>(Arrays.asList(
			"cn",
			"sn",
			"mail")));
	}


	@Override
	public boolean includesAttributesWithOptions() {
		return true;
	}


	@Override
	public RDN resolveRDN(final String key) {
		return new RDN("uid", key);
	}


	@Override
	public LDAPEntry toLDAPEntry(final DN baseDN, final InfinispanEntry<String,User> infinispanEntry) {

		DN dn = new DN(resolveRDN(infinispanEntry.getKey()), baseDN);

		Collection<Attribute> attrs = new LinkedList<>();

		attrs.add(new Attribute("objectClass", "inetOrgPerson"));
		attrs.add(new Attribute("uid", infinispanEntry.getKey()));
		attrs.add(new Attribute("cn", infinispanEntry.getValue().getName()));
		attrs.add(new Attribute("sn", infinispanEntry.getValue().getName().split("\\s")[1]));
		attrs.add(new Attribute("mail", infinispanEntry.getValue().getEmail()));

		// The metadata is ignored

		return new LDAPEntry(new ReadOnlyEntry(dn, attrs), writeStrategy);
	}


	@Override
	public InfinispanEntry<String,User> toInfinispanEntry(final LDAPEntry ldapEntry) {

		RDN rdn;
		try {
			rdn = ldapEntry.getEntry().getRDN();
		} catch (LDAPException e) {
			throw new PersistenceException(e.getMessage(), e);
		}

		if (! rdn.getAttributeNames()[0].equalsIgnoreCase("uid")) {
			throw new PersistenceException("Unexpected RDN attribute");
		}

		String key = rdn.getAttributeValues()[0];

		String name = ldapEntry.getEntry().getAttributeValue("cn");
		String email = ldapEntry.getEntry().getAttributeValue("mail");
		User user = new User(name, email);

		return new InfinispanEntry<>(key, user, new InternalMetadataBuilder().build());
	}
}
