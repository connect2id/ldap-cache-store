package com.nimbusds.infinispan.persistence.ldap;


import org.infinispan.commons.io.ByteBuffer;
import org.infinispan.marshall.core.MarshalledEntry;
import org.infinispan.marshall.core.MarshalledEntryFactory;
import org.infinispan.marshall.core.MarshalledEntryImpl;
import org.infinispan.metadata.InternalMetadata;


class MarshalledEntryFactoryImpl<K, V> implements MarshalledEntryFactory<K, V> {
	
	
	@Override
	public MarshalledEntry<K, V> newMarshalledEntry(ByteBuffer key, ByteBuffer value, ByteBuffer internalMetadata) {
		return new MarshalledEntryImpl<>(key, value, internalMetadata, null);
	}
	
	
	@Override
	public MarshalledEntry<K, V> newMarshalledEntry(Object key, ByteBuffer value, ByteBuffer internalMetadata) {
		return new MarshalledEntryImpl<>((K)key, value, internalMetadata, null);
	}
	
	
	@Override
	public MarshalledEntry<K, V> newMarshalledEntry(Object key, Object value, InternalMetadata internalMetadata) {
		return new MarshalledEntryImpl<>((K)key, (V)value, internalMetadata, null);
	}
}
