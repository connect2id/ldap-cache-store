package com.nimbusds.infinispan.persistence.ldap;


import java.util.*;

import com.nimbusds.infinispan.persistence.common.InfinispanEntry;
import com.nimbusds.infinispan.persistence.ldap.backend.LDAPConnector;

import com.unboundid.ldap.sdk.ReadOnlyEntry;
import org.infinispan.marshall.core.MarshalledEntry;
import org.infinispan.marshall.core.MarshalledEntryFactoryImpl;
import org.infinispan.persistence.spi.AdvancedCacheExpirationWriter;


public class ExpiredEntryReaperTest extends TestWithLDAPServer {


	LDAPConnector connector;
	
	
	LDAPEntryTransformer<String,String> entityTransformer;
	
	
	@Override
	public void setUp()
		throws Exception {
		
		super.setUp();
		
		LDAPStoreConfiguration config = new LDAPStoreConfiguration(getTestProperties());
		
		entityTransformer = new ExpiredEmailTransformer();
		
		connector = new LDAPConnector(
			config,
			"myCache",
			entityTransformer.getModifiableAttributes(),
			entityTransformer.includesAttributesWithOptions());
		
		for (int i=0; i < 2000; ++i) {
			
			ReadOnlyEntry ldapEntry = entityTransformer.toLDAPEntry(
				config.ldapDirectory.baseDN,
				new InfinispanEntry<>(
					i+"",
					"hello-" + i + "@email.com",
					null))
				.getEntry();
			
			connector.addEntry(ldapEntry);
		}
		
		assertEquals(2000, connector.countEntries());
	}
	
	
	public void testExpireWithKeyListener() {
		

		ExpiredEntryReaper<String,String> reaper = new ExpiredEntryReaper<>(new MarshalledEntryFactoryImpl(), connector, entityTransformer);

		List<String> deletedKeys = new LinkedList<>();

		// Purge expired entries (all must be marked as such)
		assertEquals(2000, reaper.purge(key -> {
			assertNotNull(key);
			deletedKeys.add(key);
		}));


		// Check deleted keys pushed to listener
		for (int i=0; i < 2000;++i) {
			assertTrue(deletedKeys.contains(i + ""));
		}

		assertEquals(2000, deletedKeys.size());
	}
	
	
	public void testExpireWithEntryListener() {

		LDAPStoreConfiguration config = new LDAPStoreConfiguration(getTestProperties());

		LDAPEntryTransformer<String,String> entityTransformer = new ExpiredEmailTransformer();

		LDAPConnector connector = new LDAPConnector(
			config,
			"myCache",
			entityTransformer.getModifiableAttributes(),
			entityTransformer.includesAttributesWithOptions());

		for (int i=0; i < 2000; ++i) {

			ReadOnlyEntry ldapEntry = entityTransformer.toLDAPEntry(
				config.ldapDirectory.baseDN,
				new InfinispanEntry<>(
					i+"",
					"hello-" + i + "@email.com",
					null))
				.getEntry();

			connector.addEntry(ldapEntry);
		}

		assertEquals(2000, connector.countEntries());

		ExpiredEntryReaper<String,String> reaper = new ExpiredEntryReaper<>(new MarshalledEntryFactoryImpl(), connector, entityTransformer);

		List<String> deletedKeys = new LinkedList<>();

		// Purge expired entries (all must be marked as such)
		assertEquals(2000, reaper.purgeExtended(new AdvancedCacheExpirationWriter.ExpirationPurgeListener<String, String>() {
			@Override
			public void marshalledEntryPurged(MarshalledEntry<String, String> marshalledEntry) {
				assertNotNull(marshalledEntry.getKey());
				assertNotNull(marshalledEntry.getValue());
				assertNotNull(marshalledEntry.getMetadata());
				deletedKeys.add(marshalledEntry.getKey());
			}


			@Override
			public void entryPurged(String s) {
				fail(); // should not be called
			}
		}));

		// Check deleted keys pushed to listener
		for (int i=0; i < 2000;++i) {
			assertTrue(deletedKeys.contains(i + ""));
		}

		assertEquals(2000, deletedKeys.size());
	}
}
