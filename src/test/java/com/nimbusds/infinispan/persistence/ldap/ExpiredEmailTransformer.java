package com.nimbusds.infinispan.persistence.ldap;


import java.util.*;
import java.util.concurrent.TimeUnit;

import com.nimbusds.infinispan.persistence.common.InfinispanEntry;
import com.nimbusds.infinispan.persistence.common.InternalMetadataBuilder;
import com.unboundid.ldap.sdk.Attribute;
import com.unboundid.ldap.sdk.DN;
import com.unboundid.ldap.sdk.RDN;
import com.unboundid.ldap.sdk.ReadOnlyEntry;
import org.infinispan.metadata.InternalMetadata;


class ExpiredEmailTransformer implements LDAPEntryTransformer<String,String> {
	
	
	@Override
	public Set<String> getModifiableAttributes() {
		return Collections.singleton("mail");
	}
	
	
	@Override
	public boolean includesAttributesWithOptions() {
		return false;
	}
	
	
	@Override
	public RDN resolveRDN(final String key) {
		return new RDN("uid", key);
	}
	
	
	@Override
	public LDAPEntry toLDAPEntry(final DN baseDN,
				     final InfinispanEntry<String,String> infinispanEntry) {
	
		Collection<Attribute> attrs = new LinkedList<>();
		attrs.add(new Attribute("objectClass", "inetOrgPerson"));
		attrs.add(new Attribute("uid", infinispanEntry.getKey()));
		attrs.add(new Attribute("sn", "Surname"));
		attrs.add(new Attribute("cn", "GivenName Surname"));
		attrs.add(new Attribute("mail", infinispanEntry.getValue()));
		return new LDAPEntry(new ReadOnlyEntry(new DN(resolveRDN(infinispanEntry.getKey()), baseDN), attrs));
	}
	
	
	@Override
	public InfinispanEntry<String, String> toInfinispanEntry(final LDAPEntry ldapEntry) {
		
		final Date now = new Date();
		final Date yesterday = new Date(now.getTime() - 24*60*60*1000);
		
		String key = ldapEntry.getEntry().getAttributeValue("uid");
		String value = ldapEntry.getEntry().getAttributeValue("mail");
		
		// Simulate expired entry
		InternalMetadata metadata = new InternalMetadataBuilder()
			.created(yesterday)
			.lastUsed(yesterday)
			.lifespan(6L, TimeUnit.HOURS)
			.maxIdle(1L, TimeUnit.HOURS)
			.build();
		
		assert metadata.isExpired(now.getTime());
		
		return new InfinispanEntry<>(key, value, metadata);
	}
}