package com.nimbusds.infinispan.persistence.ldap;


import net.jcip.annotations.Immutable;


/**
 * User POJO, test class.
 */
@Immutable
public final class User {
	
	private final String name;

	private final String email;


	public User(String name, String email) {
		this.name = name;
		this.email = email;
	}


	public String getName() {
		return name;
	}


	public String getEmail() {
		return email;
	}


	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof User)) return false;

		User user = (User) o;

		if (name != null ? !name.equals(user.name) : user.name != null)
			return false;
		return email != null ? email.equals(user.email) : user.email == null;

	}


	@Override
	public int hashCode() {
		int result = name != null ? name.hashCode() : 0;
		result = 31 * result + (email != null ? email.hashCode() : 0);
		return result;
	}


	@Override
	public String toString() {
		return "User{" +
			"name='" + name + '\'' +
			", email='" + email + '\'' +
			'}';
	}
}
