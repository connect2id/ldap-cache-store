package com.nimbusds.infinispan.persistence.ldap;


import java.io.FileInputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import org.infinispan.Cache;
import org.infinispan.configuration.cache.ConfigurationBuilder;
import org.infinispan.eviction.EvictionStrategy;
import org.infinispan.eviction.EvictionType;
import org.infinispan.manager.DefaultCacheManager;
import org.infinispan.manager.EmbeddedCacheManager;
import org.junit.After;
import org.junit.Before;


public class LDAPStoreExpirationTest extends TestWithLDAPServer {
	
	
	private static final String CACHE_NAME = "myMap";
	
	
	private static final int EVICTION_SIZE = 100;
	
	
	/**
	 * The Infinispan cache manager.
	 */
	private EmbeddedCacheManager cacheMgr;
	
	
	@Before
	@Override
	public void setUp()
		throws Exception {
		
		super.setUp();
		
		// Override LDAP entry transformer
		Properties props = new Properties();
		props.load(new FileInputStream("test.properties"));
		props.setProperty("ldapDirectory.entryTransformer", ExpiredEmailTransformer.class.getName());
		
		cacheMgr = new DefaultCacheManager();
		
		ConfigurationBuilder b = new ConfigurationBuilder();
		b.persistence()
			.addStore(LDAPStoreConfigurationBuilder.class)
			.withProperties(props)
			.create();
		
		b.memory()
			.evictionStrategy(EvictionStrategy.REMOVE)
			.size(EVICTION_SIZE)
			.evictionType(EvictionType.COUNT)
			.create();
		
		b.expiration()
			.wakeUpInterval(10L, TimeUnit.MINUTES) // slow down
			.create();
		
		cacheMgr.defineConfiguration(CACHE_NAME, b.build());
		
		cacheMgr.start();
	}
	
	
	@After
	@Override
	public void tearDown()
		throws Exception {
		
		super.tearDown();
		
		if (cacheMgr != null) cacheMgr.stop();
	}
	
	
	public void testDontLoadExpired() {
		
		Cache<String,String> myMap = cacheMgr.getCache(CACHE_NAME);
		
		List<String> keys = new LinkedList<>();
		
		for (int i=0; i < 200; i++) {
			String key = UUID.randomUUID().toString();
			keys.add(key);
			myMap.put(key, "alice-" + i + "@example.com");
		}
		
		assertEquals(EVICTION_SIZE, myMap.size());
		assertEquals(keys.size(), LDAPStore.getInstances().get(CACHE_NAME).size());
		
		int numFoundExpired = 0;
		for (String key: keys) {
			if (LDAPStore.getInstances().get(CACHE_NAME).load(key) == null) {
				numFoundExpired++;
			}
		}
		
		assertEquals(200, numFoundExpired);
		
		
		numFoundExpired = 0;
		for (String key: keys) {
			if (myMap.get(key) == null) {
				numFoundExpired++;
			}
		}
		
		assertEquals("In memory not affected", EVICTION_SIZE, numFoundExpired);
	}
	
	
	public void testDontProcessExpired() {
		
		Cache<String,String> myMap = cacheMgr.getCache(CACHE_NAME);
		
		List<String> keys = new LinkedList<>();
		
		for (int i=0; i < 200; i++) {
			String key = UUID.randomUUID().toString();
			keys.add(key);
			myMap.put(key, "alice-" + i + "@example.com");
		}
		
		assertEquals(EVICTION_SIZE, myMap.size());
		assertEquals(keys.size(), LDAPStore.getInstances().get(CACHE_NAME).size());
		
		final AtomicInteger numFoundExpired = new AtomicInteger();
		myMap.forEach((key1, value) -> numFoundExpired.incrementAndGet());
		
		assertEquals(100, numFoundExpired.get());
		
		assertEquals("In memory not affected", EVICTION_SIZE, numFoundExpired.get());
	}
	
	
	public void testExpire()
		throws Exception {
		
		Cache<String,String> myMap = cacheMgr.getCache(CACHE_NAME);
		
		final int count = 200;
		
		for (int i=0; i < count; i++) {
			String key = UUID.randomUUID().toString();
			myMap.put(key, "alice-" + i + "@example.com");
		}
		
		assertEquals(EVICTION_SIZE, myMap.size());
		assertEquals(count, LDAPStore.getInstances().get(CACHE_NAME).size());
		
		myMap.getAdvancedCache().getExpirationManager().processExpiration(); // purge expired in separate thread
		
		Thread.sleep(1000L);
		
		// Entries in LDAP purged
		assertEquals(myMap.getCacheConfiguration().memory().size(), myMap.size());
		assertEquals(0, LDAPStore.getInstances().get(CACHE_NAME).size());
	}
}