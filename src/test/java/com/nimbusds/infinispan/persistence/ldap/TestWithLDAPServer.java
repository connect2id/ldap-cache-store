package com.nimbusds.infinispan.persistence.ldap;


import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import com.unboundid.ldap.listener.InMemoryDirectoryServer;
import com.unboundid.ldap.listener.InMemoryDirectoryServerConfig;
import com.unboundid.ldap.listener.InMemoryListenerConfig;
import com.unboundid.ldap.sdk.Entry;
import com.unboundid.ldap.sdk.LDAPException;
import junit.framework.TestCase;


/**
 * The base class for tests that require an LDAP server.
 */
public class TestWithLDAPServer extends TestCase {
	

	/**
	 * Returns the test configuration properties.
	 *
	 * @return The test configuration properties.
	 */
	public static Properties getTestProperties() {

		Properties props = new Properties();

		try {
			props.load(new FileInputStream("test.properties"));
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		return props;
	}


	/**
	 * The test in-memory LDAP server.
	 */
	private InMemoryDirectoryServer testLDAPServer;


	/**
	 * Returns the test in-memory LDAP server.
	 *
	 * @return The test LDAP server, {@code null} if not created.
	 */
	public InMemoryDirectoryServer getTestLDAPServer() {
		return testLDAPServer;
	}


	@Override
	public void setUp()
		throws Exception {

		LDAPStoreConfiguration config = new LDAPStoreConfiguration(getTestProperties());

		// Set test LDAP server port
		InMemoryListenerConfig listenerConfig = InMemoryListenerConfig.createLDAPConfig("test-ldap", config.ldapServer.url[0].getPort());

		// Set test LDAP server context, user
		InMemoryDirectoryServerConfig dirConfig = new InMemoryDirectoryServerConfig("dc=wonderland,dc=net");
		dirConfig.setListenerConfigs(listenerConfig);
		dirConfig.addAdditionalBindCredentials(config.ldapUser.dn.toString(), config.ldapUser.password);
		dirConfig.setEnforceAttributeSyntaxCompliance(true);
		dirConfig.setMaxConnections(5);

		// Start test LDAP server
		testLDAPServer = new InMemoryDirectoryServer(dirConfig);
		testLDAPServer.startListening();

		// Populate initial directory
		testLDAPServer.bind(config.ldapUser.dn.toString(), config.ldapUser.password);

		Entry entry = new Entry("dc=wonderland,dc=net");
		entry.addAttribute("objectClass", "top", "domain");
		entry.addAttribute("dc", "wonderland");
		testLDAPServer.add(entry);

		entry = new Entry(config.ldapDirectory.baseDN);
		entry.addAttribute("objectClass", "top", "organizationalUnit");
		testLDAPServer.add(entry);
	}


	public void testLDAPServer()
		throws LDAPException {

		Entry rootEntry = getTestLDAPServer().getRootDSE();
		assertNotNull(rootEntry);
	}


	@Override
	public void tearDown()
		throws Exception {

		testLDAPServer.shutDown(true);
	}
}
