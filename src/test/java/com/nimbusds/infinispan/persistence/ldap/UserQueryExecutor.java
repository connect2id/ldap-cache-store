package com.nimbusds.infinispan.persistence.ldap;


import java.util.function.Consumer;

import com.nimbusds.infinispan.persistence.common.InfinispanEntry;
import com.nimbusds.infinispan.persistence.common.query.Query;
import com.nimbusds.infinispan.persistence.common.query.SimpleMatchQuery;
import com.nimbusds.infinispan.persistence.common.query.UnsupportedQueryException;
import com.nimbusds.infinispan.persistence.ldap.query.LDAPQueryExecutor;
import com.nimbusds.infinispan.persistence.ldap.query.LDAPQueryExecutorInitContext;
import com.unboundid.ldap.sdk.*;


public class UserQueryExecutor implements LDAPQueryExecutor<String,User> {
	
	
	LDAPQueryExecutorInitContext<String,User> initCtx;
	
	
	@Override
	public void init(LDAPQueryExecutorInitContext<String,User> initCtx) {
		this.initCtx = initCtx;
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public void executeQuery(Query query, Consumer<InfinispanEntry<String,User>> consumer) {
		if (! (query instanceof SimpleMatchQuery))
			throw new UnsupportedQueryException(query);
		
		SimpleMatchQuery<String,String> matchQuery = (SimpleMatchQuery<String,String>)query;
		
		// Only searches for mail supported
		if (! matchQuery.getKey().equals("mail"))
			throw new UnsupportedQueryException(query);
			
		Filter filter = Filter.createEqualityFilter(matchQuery.getKey(), matchQuery.getValue());
		
		initCtx.getLDAPConnector().retrieveEntries(
			filter, readOnlyEntry -> consumer.accept(
			initCtx.getLDAPEntryTransformer().toInfinispanEntry(new LDAPEntry(readOnlyEntry))));
	}
}
