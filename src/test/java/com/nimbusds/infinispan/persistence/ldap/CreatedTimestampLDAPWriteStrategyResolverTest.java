package com.nimbusds.infinispan.persistence.ldap;


import java.util.Date;

import com.nimbusds.infinispan.persistence.common.InfinispanEntry;
import com.nimbusds.infinispan.persistence.common.InternalMetadataBuilder;
import junit.framework.TestCase;


public class CreatedTimestampLDAPWriteStrategyResolverTest extends TestCase {
	

	public void testConstant() {

		assertEquals(50L, CreatedTimestampLDAPWriteStrategyResolver.DEFAULT_RECENT_AGE_MS);
	}


	@SuppressWarnings("unchecked")
	public void testDefaultConstructor() {

		CreatedTimestampLDAPWriteStrategyResolver resolver = new CreatedTimestampLDAPWriteStrategyResolver();

		InfinispanEntry nullMetaEntry = new InfinispanEntry("key", "value", null);
		assertEquals(LDAPWriteStrategy.TRY_LDAP_ADD_FIRST, resolver.resolveLDAPWriteStrategy(nullMetaEntry));

		InfinispanEntry noTimestampEntry = new InfinispanEntry("key", "value", new InternalMetadataBuilder().build());
		assertEquals(LDAPWriteStrategy.TRY_LDAP_ADD_FIRST, resolver.resolveLDAPWriteStrategy(noTimestampEntry));

		Date now = new Date();

		InfinispanEntry recentEntry = new InfinispanEntry("key", "value", new InternalMetadataBuilder().created(now.getTime()).build());
		assertEquals(LDAPWriteStrategy.TRY_LDAP_ADD_FIRST, resolver.resolveLDAPWriteStrategy(recentEntry));

		InfinispanEntry oldEntry = new InfinispanEntry("key", "value", new InternalMetadataBuilder().created(now.getTime() - 60).build());
		assertEquals(LDAPWriteStrategy.TRY_LDAP_MODIFY_FIRST, resolver.resolveLDAPWriteStrategy(oldEntry));
	}


	@SuppressWarnings("unchecked")
	public void testSpecConstructor() {

		CreatedTimestampLDAPWriteStrategyResolver resolver = new CreatedTimestampLDAPWriteStrategyResolver(1000L); // 1 second

		InfinispanEntry nullMetaEntry = new InfinispanEntry("key", "value", null);
		assertEquals(LDAPWriteStrategy.TRY_LDAP_ADD_FIRST, resolver.resolveLDAPWriteStrategy(nullMetaEntry));

		InfinispanEntry noTimestampEntry = new InfinispanEntry("key", "value", new InternalMetadataBuilder().build());
		assertEquals(LDAPWriteStrategy.TRY_LDAP_ADD_FIRST, resolver.resolveLDAPWriteStrategy(noTimestampEntry));

		Date now = new Date();

		InfinispanEntry recentEntry = new InfinispanEntry("key", "value", new InternalMetadataBuilder().created(now.getTime() - 900L).build());
		assertEquals(LDAPWriteStrategy.TRY_LDAP_ADD_FIRST, resolver.resolveLDAPWriteStrategy(recentEntry));

		InfinispanEntry oldEntry = new InfinispanEntry("key", "value", new InternalMetadataBuilder().created(now.getTime() - 1100L).build());
		assertEquals(LDAPWriteStrategy.TRY_LDAP_MODIFY_FIRST, resolver.resolveLDAPWriteStrategy(oldEntry));
	}
}
