package com.nimbusds.infinispan.persistence.ldap;


import java.io.FileInputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;
import com.nimbusds.common.monitor.MonitorRegistries;
import com.nimbusds.infinispan.persistence.common.query.Query;
import com.nimbusds.infinispan.persistence.common.query.QueryExecutor;
import com.nimbusds.infinispan.persistence.common.query.SimpleMatchQuery;
import com.unboundid.ldap.sdk.LDAPException;
import com.unboundid.ldap.sdk.ReadOnlyEntry;
import org.infinispan.Cache;
import org.infinispan.configuration.cache.ConfigurationBuilder;
import org.infinispan.context.Flag;
import org.infinispan.eviction.EvictionStrategy;
import org.infinispan.eviction.EvictionType;
import org.infinispan.manager.DefaultCacheManager;
import org.infinispan.manager.EmbeddedCacheManager;
import org.infinispan.persistence.spi.PersistenceException;
import org.junit.After;
import org.junit.Before;


/**
 * Tests the LDAP store with a programmatic config.
 */
public class LDAPStoreWithProgConfigTest extends TestWithLDAPServer {


	public static final String CACHE_NAME = "myMap";


	/**
	 * The Infinispan cache manager.
	 */
	protected EmbeddedCacheManager cacheMgr;


	@Before
	@Override
	public void setUp()
		throws Exception {

		Properties props = new Properties();
		props.load(new FileInputStream("test.properties"));
		setUp(props);
	}


	public void setUp(final Properties props)
		throws Exception {

		super.setUp();

		cacheMgr = new DefaultCacheManager();

		ConfigurationBuilder b = new ConfigurationBuilder();
		b.persistence()
			.addStore(LDAPStoreConfigurationBuilder.class)
			.withProperties(props)
			.create();
		
		b.memory()
			.evictionStrategy(EvictionStrategy.REMOVE)
			.size(100)
			.evictionType(EvictionType.COUNT)
			.create();

		b.expiration()
			.wakeUpInterval(50L, TimeUnit.MILLISECONDS)
			.create();

		cacheMgr.defineConfiguration(CACHE_NAME, b.build());
		System.out.println("Applied LDAP external store configuration to " + CACHE_NAME + " cache");

		cacheMgr.start();
		System.out.println("Started Infinispan cache manager");
	}


	@After
	@Override
	public void tearDown()
		throws Exception {

		super.tearDown();

		if (cacheMgr != null) cacheMgr.stop();
	}


	public void testSimpleObjectLifeCycle()
		throws Exception {

		assertNull(UserEntryTransformer.getWriteStrategy());

		Cache<String,User> myMap = cacheMgr.getCache(CACHE_NAME);
		
		assertNotNull(LDAPStore.getInstances().get(CACHE_NAME));
		assertEquals(1, LDAPStore.getInstances().size());

		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.getTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.searchTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.addTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.modifyTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.deleteTimer").getCount());

		// Initial size
		assertEquals(0, myMap.size());

		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.getTimer").getCount());
		assertTrue(MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.searchTimer").getCount() >= 1L);
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.addTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.modifyTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.deleteTimer").getCount());

		// Get non-existing object
		assertNull(myMap.get("invalid-key"));

		assertEquals(1L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.getTimer").getCount());
		assertTrue(MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.searchTimer").getCount() >= 1L);
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.addTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.modifyTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.deleteTimer").getCount());

		// Store new object
		String k1 = "alice";
		User u1 = new User("Alice Adams", "alice@wonderland.net");

		assertNull(myMap.putIfAbsent(k1, u1));

		assertEquals(2L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.getTimer").getCount());
		assertTrue(MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.searchTimer").getCount() >= 1L);
		assertEquals(1L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.addTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.modifyTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.deleteTimer").getCount());

		// Check presence
		assertTrue(myMap.containsKey(k1));

		assertEquals(2L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.getTimer").getCount());
		assertTrue(MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.searchTimer").getCount() >= 1L);
		assertEquals(1L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.addTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.modifyTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.deleteTimer").getCount());

		// Confirm LDAP write
		ReadOnlyEntry u1Entry = getTestLDAPServer().getEntry("uid="+k1+",ou=people,dc=wonderland,dc=net");
		assertEquals("Alice Adams",  u1Entry.getAttributeValue("cn"));
		assertEquals(1, u1Entry.getAttribute("cn").size());
		assertEquals("Adams",  u1Entry.getAttributeValue("sn"));
		assertEquals(1, u1Entry.getAttribute("sn").size());
		assertEquals("alice@wonderland.net",  u1Entry.getAttributeValue("mail"));
		assertEquals(1, u1Entry.getAttribute("mail").size());

		// Get new count
		assertEquals(1, myMap.size());

		assertEquals(2L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.getTimer").getCount());
		assertTrue(MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.searchTimer").getCount() >= 2L);
		assertEquals(1L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.addTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.modifyTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.deleteTimer").getCount());

		// Get object
		assertEquals("Alice Adams", myMap.get(k1).getName());
		assertEquals("alice@wonderland.net", myMap.get(k1).getEmail());

		assertEquals(2L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.getTimer").getCount());
		assertTrue(MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.searchTimer").getCount() >= 2L);
		assertEquals(1L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.addTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.modifyTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.deleteTimer").getCount());

		// Update object
		u1 = new User("Bob Brown", "bob@wonderland.net");
		assertNotNull(myMap.replace(k1, u1));

		assertEquals(3L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.getTimer").getCount());
		assertTrue(MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.searchTimer").getCount() >= 2L);
		assertEquals(2L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.addTimer").getCount());
		assertEquals(1L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.modifyTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.deleteTimer").getCount());

		// Confirm LDAP modify
		u1Entry = getTestLDAPServer().getEntry("uid="+k1+",ou=people,dc=wonderland,dc=net");
		assertEquals("Bob Brown",  u1Entry.getAttributeValue("cn"));
		assertEquals(1, u1Entry.getAttribute("cn").size());
		assertEquals("Brown",  u1Entry.getAttributeValue("sn"));
		assertEquals(1, u1Entry.getAttribute("sn").size());
		assertEquals("bob@wonderland.net",  u1Entry.getAttributeValue("mail"));
		assertEquals(1, u1Entry.getAttribute("mail").size());

		// Get object
		assertEquals("Bob Brown", myMap.get(k1).getName());
		assertEquals("bob@wonderland.net", myMap.get(k1).getEmail());

		assertEquals(3L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.getTimer").getCount());
		assertTrue(MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.searchTimer").getCount() >= 2L);
		assertEquals(2L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.addTimer").getCount());
		assertEquals(1L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.modifyTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.deleteTimer").getCount());

		// Remove object
		u1 = myMap.remove(k1);
		assertEquals("Bob Brown", u1.getName());
		assertEquals("bob@wonderland.net", u1.getEmail());

		assertEquals(3L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.getTimer").getCount());
		assertTrue(MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.searchTimer").getCount() >= 2L);
		assertEquals(2L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.addTimer").getCount());
		assertEquals(1L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.modifyTimer").getCount());
		assertEquals(1L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.deleteTimer").getCount());

		// Confirm removal
		assertNull(myMap.get(k1));
		assertFalse(myMap.containsKey(k1));

		assertEquals(5L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.getTimer").getCount());
		assertTrue(MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.searchTimer").getCount() >= 2L);
		assertEquals(2L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.addTimer").getCount());
		assertEquals(1L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.modifyTimer").getCount());
		assertEquals(1L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.deleteTimer").getCount());

		// Confirm LDAP delete
		assertNull(getTestLDAPServer().getEntry("uid="+k1+",ou=people,dc=wonderland,dc=net"));

		// Zero count
		assertEquals(0, myMap.size());
		assertEquals(0, myMap.getAdvancedCache().size());
		assertEquals(0, myMap.getAdvancedCache().getDataContainer().size());

		assertEquals(5L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.getTimer").getCount());
		assertTrue(MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.searchTimer").getCount() >= 4L);
		assertEquals(2L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.addTimer").getCount());
		assertEquals(1L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.modifyTimer").getCount());
		assertEquals(1L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.deleteTimer").getCount());
	}


	public void testSimpleObjectLifeCycle_tryLDAPModifyFirst()
		throws Exception {

		UserEntryTransformer.setWriteStrategy(LDAPWriteStrategy.TRY_LDAP_MODIFY_FIRST);

		Cache<String,User> myMap = cacheMgr.getCache(CACHE_NAME);

		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.getTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.searchTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.addTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.modifyTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.deleteTimer").getCount());

		// Initial size
		assertEquals(0, myMap.size());

		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.getTimer").getCount());
		assertTrue(MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.searchTimer").getCount() >= 1L);
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.addTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.modifyTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.deleteTimer").getCount());

		// Get non-existing object
		assertNull(myMap.get("invalid-key"));

		assertEquals(1L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.getTimer").getCount());
		assertTrue(MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.searchTimer").getCount() >= 1L);
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.addTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.modifyTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.deleteTimer").getCount());

		// Store new object
		String k1 = "alice";
		User u1 = new User("Alice Adams", "alice@wonderland.net");
		assertNull(myMap.putIfAbsent(k1, u1));

		assertEquals(3L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.getTimer").getCount());
		assertTrue(MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.searchTimer").getCount() >= 1L);
		assertEquals(1L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.addTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.modifyTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.deleteTimer").getCount());

		// Check presence
		assertTrue(myMap.containsKey(k1));

		assertEquals(3L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.getTimer").getCount());
		assertTrue(MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.searchTimer").getCount() >= 1L);
		assertEquals(1L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.addTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.modifyTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.deleteTimer").getCount());

		// Confirm LDAP write
		ReadOnlyEntry u1Entry = getTestLDAPServer().getEntry("uid="+k1+",ou=people,dc=wonderland,dc=net");
		assertEquals("Alice Adams",  u1Entry.getAttributeValue("cn"));
		assertEquals(1, u1Entry.getAttribute("cn").size());
		assertEquals("Adams",  u1Entry.getAttributeValue("sn"));
		assertEquals(1, u1Entry.getAttribute("sn").size());
		assertEquals("alice@wonderland.net",  u1Entry.getAttributeValue("mail"));
		assertEquals(1, u1Entry.getAttribute("mail").size());

		// Get new count
		assertEquals(1, myMap.size());

		assertEquals(3L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.getTimer").getCount());
		assertTrue(MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.searchTimer").getCount() >= 2L);
		assertEquals(1L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.addTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.modifyTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.deleteTimer").getCount());

		// Get object
		assertEquals("Alice Adams", myMap.get(k1).getName());
		assertEquals("alice@wonderland.net", myMap.get(k1).getEmail());

		assertEquals(3L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.getTimer").getCount());
		assertTrue(MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.searchTimer").getCount() >= 2L);
		assertEquals(1L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.addTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.modifyTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.deleteTimer").getCount());

		// Update object
		u1 = new User("Bob Brown", "bob@wonderland.net");
		assertNotNull(myMap.replace(k1, u1));

		assertEquals(4L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.getTimer").getCount());
		assertTrue(MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.searchTimer").getCount() >= 2L);
		assertEquals(1L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.addTimer").getCount());
		assertEquals(1L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.modifyTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.deleteTimer").getCount());

		// Confirm LDAP modify
		u1Entry = getTestLDAPServer().getEntry("uid="+k1+",ou=people,dc=wonderland,dc=net");
		assertEquals("Bob Brown",  u1Entry.getAttributeValue("cn"));
		assertEquals(1, u1Entry.getAttribute("cn").size());
		assertEquals("Brown",  u1Entry.getAttributeValue("sn"));
		assertEquals(1, u1Entry.getAttribute("sn").size());
		assertEquals("bob@wonderland.net",  u1Entry.getAttributeValue("mail"));
		assertEquals(1, u1Entry.getAttribute("mail").size());

		// Get object
		assertEquals("Bob Brown", myMap.get(k1).getName());
		assertEquals("bob@wonderland.net", myMap.get(k1).getEmail());

		assertEquals(4L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.getTimer").getCount());
		assertTrue(MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.searchTimer").getCount() >= 2L);
		assertEquals(1L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.addTimer").getCount());
		assertEquals(1L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.modifyTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.deleteTimer").getCount());

		// Remove object
		u1 = myMap.remove(k1);
		assertEquals("Bob Brown", u1.getName());
		assertEquals("bob@wonderland.net", u1.getEmail());

		assertEquals(4L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.getTimer").getCount());
		assertTrue(MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.searchTimer").getCount() >= 2L);
		assertEquals(1L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.addTimer").getCount());
		assertEquals(1L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.modifyTimer").getCount());
		assertEquals(1L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.deleteTimer").getCount());

		// Confirm removal
		assertNull(myMap.get(k1));
		assertFalse(myMap.containsKey(k1));

		assertEquals(6L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.getTimer").getCount());
		assertTrue(MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.searchTimer").getCount() >= 2L);
		assertEquals(1L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.addTimer").getCount());
		assertEquals(1L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.modifyTimer").getCount());
		assertEquals(1L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.deleteTimer").getCount());

		// Confirm LDAP delete
		assertNull(getTestLDAPServer().getEntry("uid="+k1+",ou=people,dc=wonderland,dc=net"));

		// Zero count
		assertEquals(0, myMap.size());
		assertEquals(0, myMap.getAdvancedCache().size());
		assertEquals(0, myMap.getAdvancedCache().getDataContainer().size());

		assertEquals(6L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.getTimer").getCount());
		assertTrue(MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.searchTimer").getCount() >= 4L);
		assertEquals(1L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.addTimer").getCount());
		assertEquals(1L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.modifyTimer").getCount());
		assertEquals(1L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.deleteTimer").getCount());
	}


	public void testMultiplePutIfAbsentWithEviction()
		throws Exception {

		Cache<String, User> myMap = cacheMgr.getCache(CACHE_NAME);

		assertEquals(100, myMap.getCacheConfiguration().memory().size());
		assertEquals(EvictionType.COUNT, myMap.getCacheConfiguration().memory().evictionType());
		assertEquals(50, myMap.getCacheConfiguration().expiration().wakeUpInterval());

		String[] keys = new String[200];

		for (int i=0; i < 200; i ++) {

			String key = "a" + i;
			keys[i] = key;
			User value = new User("Alice Adams-" + i, "alice-" + i + "@email.com");

			assertNull(myMap.putIfAbsent(key, value));
		}

		// Confirm LDAP write
		for (int i=0; i < 200; i ++) {

			ReadOnlyEntry entry = getTestLDAPServer().getEntry("uid="+keys[i]+",ou=people,dc=wonderland,dc=net");
			assertEquals("Alice Adams-" + i,  entry.getAttributeValue("cn"));
			assertEquals(1, entry.getAttribute("cn").size());
			assertEquals("Adams-" + i,  entry.getAttributeValue("sn"));
			assertEquals(1, entry.getAttribute("sn").size());
			assertEquals("alice-"+i+"@email.com",  entry.getAttributeValue("mail"));
			assertEquals(1, entry.getAttribute("mail").size());
		}

		// Calls AdvancedCacheLoader.process, result matches eviction size
		assertEquals(100, myMap.getAdvancedCache().getDataContainer().size());

		// Calls AdvancedCacheLoader.process, result equals total persisted
		assertEquals(200, myMap.size());

		// Check presence
		for (int i=0; i < 200; i ++) {
			assertTrue(myMap.containsKey(keys[i]));
		}

		assertEquals(200, myMap.size());

		// Evict entries from memory, LDAP store unaffected
		for (String uuid: keys) {
			myMap.evict(uuid);
		}

		assertEquals(200, myMap.size());

		// Recheck presence, loads data from LDAP
		for (int i=0; i < 200; i ++) {
			assertTrue(myMap.containsKey(keys[i]));
		}

		// Confirm LDAP presence
		for (int i=0; i < 200; i ++) {

			ReadOnlyEntry entry = getTestLDAPServer().getEntry("uid="+keys[i]+",ou=people,dc=wonderland,dc=net");
			assertEquals("Alice Adams-" + i,  entry.getAttributeValue("cn"));
			assertEquals(1, entry.getAttribute("cn").size());
			assertEquals("Adams-" + i,  entry.getAttributeValue("sn"));
			assertEquals(1, entry.getAttribute("sn").size());
			assertEquals("alice-"+i+"@email.com",  entry.getAttributeValue("mail"));
			assertEquals(1, entry.getAttribute("mail").size());
		}

		// Evict entries from memory, LDAP store unaffected
		for (String uuid: keys) {
			myMap.evict(uuid);
		}

		assertEquals(200, myMap.size());

		// Iterate and count, calls AdvancedCacheLoader.process
		AtomicInteger counter = new AtomicInteger();
		myMap.keySet().iterator().forEachRemaining(uuid -> counter.incrementAndGet());
		assertEquals(200, counter.get());

		// Remove from cache and LDAP store
		for (int i=0; i < 200; i ++) {
			assertNotNull(myMap.remove(keys[i]));
		}

		assertEquals(0, myMap.size());

		// Confirm LDAP deletion
		for (int i=0; i < 200; i ++) {

			assertNull(getTestLDAPServer().getEntry("uid="+keys[i]+",ou=people,dc=wonderland,dc=net"));
		}

		// Confirm deletion
		for (int i=0; i < 200; i ++) {

			assertNull(myMap.get(keys[0]));
		}
	}


	public void testPutWithLDAPServerDown() {

		Cache<String,User> myMap = cacheMgr.getCache(CACHE_NAME);

		getTestLDAPServer().shutDown(true);

		try {
			myMap.put("alice", new User("Alice Adams", "alice@name.com"));
			fail();
		} catch (PersistenceException e) {
			assertTrue(e.getMessage().startsWith("LDAP get of uid=alice,ou=people,dc=wonderland,dc=net failed:"));
			LDAPException ldapException = (LDAPException)e.getCause();
			int code = ldapException.getResultCode().intValue();
			assertTrue(code == 81 || code == 91);
		}
	}


	public void testPutAsyncWithLDAPServerDown()
		throws Exception {

		Cache<String,User> myMap = cacheMgr.getCache(CACHE_NAME);

		getTestLDAPServer().shutDown(true);

		// No exception thrown here, instead in future
		CompletableFuture<User> future = myMap.putAsync("alice", new User("Alice Adams", "alice@name.com"));

		while (! future.isDone()) {
			TimeUnit.MILLISECONDS.sleep(10);
		}

		try {
			future.get();
			fail();
		} catch (ExecutionException e) {
			assertTrue(e.getMessage().startsWith("org.infinispan.persistence.spi.PersistenceException: LDAP get of uid=alice,ou=people,dc=wonderland,dc=net failed:"));
			PersistenceException persistenceException = (PersistenceException)e.getCause();
			LDAPException ldapException = (LDAPException)persistenceException.getCause();
			int code = ldapException.getResultCode().intValue();
			assertTrue(code == 81 || code == 91);
		}

		assertEquals(0, myMap.getAdvancedCache().withFlags(Flag.SKIP_CACHE_LOAD).size());
	}


	public void testKeyFilterProcess() {

		Cache<String,User> myMap = cacheMgr.getCache(CACHE_NAME);

		// Initial state empty
		Map<String,User> filteredMap = myMap
			.entrySet()
			.stream()
			.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

		assertTrue(filteredMap.isEmpty());

		// Put entry
		String k1 = "alice";
		User u1 = new User("Alice Adams", "alice@name.com");
		myMap.put(k1, u1);

		// Confirm put
		assertEquals(u1.getName(), myMap.get(k1).getName());
		assertEquals(u1.getEmail(), myMap.get(k1).getEmail());

		// Filter all
		filteredMap = myMap
			.entrySet()
			.stream()
			.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

		assertEquals(u1.getName(), filteredMap.get(k1).getName());
		assertEquals(u1.getEmail(), filteredMap.get(k1).getEmail());
		assertEquals(1, filteredMap.size());

		// Put another entry
		String k2 = "bob";
		User u2 = new User("Bob Brown", "bob@name.com");
		myMap.put(k2, u2);

		// Filter on name
		filteredMap = myMap
			.entrySet()
			.stream()
			.filter(uuidUserEntry -> uuidUserEntry.getValue().getName().equals("Bob Brown"))
			.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

		assertEquals(u2.getName(), filteredMap.get(k2).getName());
		assertEquals(u2.getEmail(), filteredMap.get(k2).getEmail());
		assertEquals(1, filteredMap.size());

		// Filter to include all
		filteredMap = myMap
			.entrySet()
			.stream()
			.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

		assertEquals(u1.getName(), filteredMap.get(k1).getName());
		assertEquals(u1.getEmail(), filteredMap.get(k1).getEmail());
		assertEquals(u2.getName(), filteredMap.get(k2).getName());
		assertEquals(u2.getEmail(), filteredMap.get(k2).getEmail());
		assertEquals(2, filteredMap.size());
	}


	public void testRepeatPut() {

		Cache<String,User> myMap = cacheMgr.getCache(CACHE_NAME);

		String k1 = "alice";
		User u1 = new User("Alice Adams", "alice@name.com");

		// First put
		myMap.put(k1, u1);

		assertEquals(u1.getName(), myMap.get(k1).getName());
		assertEquals(u1.getEmail(), myMap.get(k1).getEmail());

		// Repeat put
		myMap.put(k1, u1);

		assertEquals(u1.getName(), myMap.get(k1).getName());
		assertEquals(u1.getEmail(), myMap.get(k1).getEmail());
	}


	public void testPutExpiring() {

		Cache<String,User> myMap = cacheMgr.getCache(CACHE_NAME);

		String k1 = "alice";
		User u1 = new User("Alice Adams", "alice@name.com");

		myMap.put(k1, u1, 60L, TimeUnit.HOURS);

		assertEquals(u1.getName(), myMap.get(k1).getName());
		assertEquals(u1.getEmail(), myMap.get(k1).getEmail());

		myMap.put(k1, u1, 60L, TimeUnit.HOURS);
	}


	public void testEscapedRDN()
		throws Exception {

		assertNull(UserEntryTransformer.getWriteStrategy());

		Cache<String,User> myMap = cacheMgr.getCache(CACHE_NAME);

		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.getTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.searchTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.addTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.modifyTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.deleteTimer").getCount());

		// Initial size
		assertEquals(0, myMap.size());

		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.getTimer").getCount());
		assertTrue(MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.searchTimer").getCount() >= 1L);
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.addTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.modifyTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.deleteTimer").getCount());

		// Get non-existing object
		assertNull(myMap.get("no-such-key"));

		assertEquals(1L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.getTimer").getCount());
		assertTrue(MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.searchTimer").getCount() >= 1L);
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.addTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.modifyTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.deleteTimer").getCount());

		// Store new object
		String k1 = "alice\n";
		User u1 = new User("Alice Adams", "alice@wonderland.net");

		assertNull(myMap.putIfAbsent(k1, u1));

		assertEquals(2L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.getTimer").getCount());
		assertTrue(MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.searchTimer").getCount() >= 1L);
		assertEquals(1L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.addTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.modifyTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.deleteTimer").getCount());

		// Check presence
		assertTrue(myMap.containsKey(k1));

		assertEquals(2L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.getTimer").getCount());
		assertTrue(MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.searchTimer").getCount() >= 1L);
		assertEquals(1L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.addTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.modifyTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.deleteTimer").getCount());

		// Confirm LDAP write
		ReadOnlyEntry u1Entry = getTestLDAPServer().getEntry("uid="+k1+",ou=people,dc=wonderland,dc=net");
		assertEquals("Alice Adams",  u1Entry.getAttributeValue("cn"));
		assertEquals(1, u1Entry.getAttribute("cn").size());
		assertEquals("Adams",  u1Entry.getAttributeValue("sn"));
		assertEquals(1, u1Entry.getAttribute("sn").size());
		assertEquals("alice@wonderland.net",  u1Entry.getAttributeValue("mail"));
		assertEquals(1, u1Entry.getAttribute("mail").size());

		// Get new count
		assertEquals(1, myMap.size());

		assertEquals(2L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.getTimer").getCount());
		assertTrue(MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.searchTimer").getCount() >= 2L);
		assertEquals(1L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.addTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.modifyTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.deleteTimer").getCount());

		// Get object
		assertEquals("Alice Adams", myMap.get(k1).getName());
		assertEquals("alice@wonderland.net", myMap.get(k1).getEmail());

		assertEquals(2L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.getTimer").getCount());
		assertTrue(MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.searchTimer").getCount() >= 2L);
		assertEquals(1L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.addTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.modifyTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.deleteTimer").getCount());

		// Update object
		u1 = new User("Bob Brown", "bob@wonderland.net");
		assertNotNull(myMap.replace(k1, u1));

		assertEquals(3L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.getTimer").getCount());
		assertTrue(MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.searchTimer").getCount() >= 2L);
		assertEquals(2L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.addTimer").getCount());
		assertEquals(1L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.modifyTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.deleteTimer").getCount());

		// Confirm LDAP modify
		u1Entry = getTestLDAPServer().getEntry("uid="+k1+",ou=people,dc=wonderland,dc=net");
		assertEquals("Bob Brown",  u1Entry.getAttributeValue("cn"));
		assertEquals(1, u1Entry.getAttribute("cn").size());
		assertEquals("Brown",  u1Entry.getAttributeValue("sn"));
		assertEquals(1, u1Entry.getAttribute("sn").size());
		assertEquals("bob@wonderland.net",  u1Entry.getAttributeValue("mail"));
		assertEquals(1, u1Entry.getAttribute("mail").size());

		// Get object
		assertEquals("Bob Brown", myMap.get(k1).getName());
		assertEquals("bob@wonderland.net", myMap.get(k1).getEmail());

		assertEquals(3L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.getTimer").getCount());
		assertTrue(MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.searchTimer").getCount() >= 2L);
		assertEquals(2L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.addTimer").getCount());
		assertEquals(1L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.modifyTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.deleteTimer").getCount());

		// Remove object
		u1 = myMap.remove(k1);
		assertEquals("Bob Brown", u1.getName());
		assertEquals("bob@wonderland.net", u1.getEmail());

		assertEquals(3L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.getTimer").getCount());
		assertTrue(MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.searchTimer").getCount() >= 2L);
		assertEquals(2L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.addTimer").getCount());
		assertEquals(1L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.modifyTimer").getCount());
		assertEquals(1L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.deleteTimer").getCount());

		// Confirm removal
		assertNull(myMap.get(k1));
		assertFalse(myMap.containsKey(k1));

		assertEquals(5L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.getTimer").getCount());
		assertTrue(MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.searchTimer").getCount() >= 2L);
		assertEquals(2L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.addTimer").getCount());
		assertEquals(1L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.modifyTimer").getCount());
		assertEquals(1L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.deleteTimer").getCount());

		// Confirm LDAP delete
		assertNull(getTestLDAPServer().getEntry("uid="+k1+",ou=people,dc=wonderland,dc=net"));

		// Zero count
		assertEquals(0, myMap.size());
		assertEquals(0, myMap.getAdvancedCache().size());
		assertEquals(0, myMap.getAdvancedCache().getDataContainer().size());

		assertEquals(5L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.getTimer").getCount());
		assertTrue(MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.searchTimer").getCount() >= 4L);
		assertEquals(2L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.addTimer").getCount());
		assertEquals(1L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.modifyTimer").getCount());
		assertEquals(1L, MonitorRegistries.getMetricRegistry().getTimers().get(CACHE_NAME + ".ldapStore.deleteTimer").getCount());
	}
	
	
	@SuppressWarnings("unchecked")
	public void testSearchQueryExecutor() {
		
		Cache<String,User> myMap = cacheMgr.getCache(CACHE_NAME);
		
		QueryExecutor<String,User> queryExecutor = (QueryExecutor<String, User>) LDAPStore.getInstances().get(CACHE_NAME).getQueryExecutor();
		
		Query query = new SimpleMatchQuery<>("mail", "alice@wonderland.net");
		
		queryExecutor.executeQuery(query, stringUserInfinispanEntry -> fail("No matches expected!"));
		
		myMap.put("a", new User("Alice Adams", "alice@wonderland.net"));
		
		List<User> matches = new LinkedList<>();
		
		queryExecutor.executeQuery(query, infinispanEntry -> matches.add(infinispanEntry.getValue()));
		
		assertEquals("Alice Adams", matches.get(0).getName());
		assertEquals("alice@wonderland.net", matches.get(0).getEmail());
		assertEquals(1, matches.size());
		
		myMap.clear();
		
		queryExecutor.executeQuery(query, stringUserInfinispanEntry -> fail("No matches expected!"));
	}
	
	
	public void testMetrics() {
		
		Cache<String,User> myMap = cacheMgr.getCache(CACHE_NAME);
		
		MetricRegistry metricRegistry = MonitorRegistries.getMetricRegistry();
		
		Map<String,Timer> timers = metricRegistry.getTimers();
		
		assertTrue(timers.containsKey(CACHE_NAME+".ldapStore.addTimer"));
		assertTrue(timers.containsKey(CACHE_NAME+".ldapStore.getTimer"));
		assertTrue(timers.containsKey(CACHE_NAME+".ldapStore.modifyTimer"));
		assertTrue(timers.containsKey(CACHE_NAME+".ldapStore.searchTimer"));
		assertTrue(timers.containsKey(CACHE_NAME+".ldapStore.deleteTimer"));
		assertEquals(5, timers.size());
	}
}