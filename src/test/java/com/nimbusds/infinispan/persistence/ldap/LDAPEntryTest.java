package com.nimbusds.infinispan.persistence.ldap;


import com.unboundid.ldap.sdk.Attribute;
import com.unboundid.ldap.sdk.ReadOnlyEntry;
import junit.framework.TestCase;


public class LDAPEntryTest extends TestCase {


	public void testFullSpec() {

		ReadOnlyEntry roEntry = new ReadOnlyEntry("cn=alice", new Attribute("cn", "Alice"), new Attribute("objectClass", "inetOrgPerson"));

		LDAPEntry ldapEntry = new LDAPEntry(roEntry, LDAPWriteStrategy.TRY_LDAP_ADD_FIRST);

		assertEquals(roEntry, ldapEntry.getEntry());
		assertEquals(LDAPWriteStrategy.TRY_LDAP_ADD_FIRST, ldapEntry.getWriteStrategy());
	}
	

	public void testNoWriteStrategySpecified() {

		ReadOnlyEntry roEntry = new ReadOnlyEntry("cn=alice", new Attribute("cn", "Alice"), new Attribute("objectClass", "inetOrgPerson"));

		LDAPEntry ldapEntry = new LDAPEntry(roEntry, null);

		assertEquals(roEntry, ldapEntry.getEntry());
		assertNull(ldapEntry.getWriteStrategy());
	}


	public void testMinimalConstructor() {

		ReadOnlyEntry roEntry = new ReadOnlyEntry("cn=alice", new Attribute("cn", "Alice"), new Attribute("objectClass", "inetOrgPerson"));

		LDAPEntry ldapEntry = new LDAPEntry(roEntry);

		assertEquals(roEntry, ldapEntry.getEntry());
		assertNull(ldapEntry.getWriteStrategy());
	}
}
