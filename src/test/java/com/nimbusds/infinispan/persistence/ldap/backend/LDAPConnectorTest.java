package com.nimbusds.infinispan.persistence.ldap.backend;


import java.util.*;

import com.nimbusds.common.monitor.MonitorRegistries;
import com.nimbusds.infinispan.persistence.ldap.LDAPStoreConfiguration;
import com.nimbusds.infinispan.persistence.ldap.TestWithLDAPServer;
import com.unboundid.ldap.sdk.Attribute;
import com.unboundid.ldap.sdk.DN;
import com.unboundid.ldap.sdk.Entry;
import com.unboundid.ldap.sdk.ReadOnlyEntry;
import org.infinispan.persistence.spi.PersistenceException;


public class LDAPConnectorTest extends TestWithLDAPServer {


	private static final String CACHE_NAME = "myCache";


	private static final Set<String> ATTRIBUTES = new HashSet<>(Arrays.asList(
		"cn",
		"sn",
		"l",
		"mail",
		"jpegPhoto"
	));
	

	public void testRun()
		throws Exception {

		LDAPStoreConfiguration config = new LDAPStoreConfiguration(getTestProperties());

		LDAPConnector ldapConnector = new LDAPConnector(config, CACHE_NAME, ATTRIBUTES, false);

		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get("myCache.ldapStore.getTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get("myCache.ldapStore.searchTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get("myCache.ldapStore.addTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get("myCache.ldapStore.modifyTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get("myCache.ldapStore.deleteTimer").getCount());

		// Some DN
		DN aliceDN = new DN("uid=alice,ou=people,dc=wonderland,dc=net");

		// Get non-existing entry
		assertNull(ldapConnector.retrieveEntry(aliceDN));

		assertEquals(1L, MonitorRegistries.getMetricRegistry().getTimers().get("myCache.ldapStore.getTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get("myCache.ldapStore.searchTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get("myCache.ldapStore.addTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get("myCache.ldapStore.modifyTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get("myCache.ldapStore.deleteTimer").getCount());

		// Check non-existing entry
		assertFalse(ldapConnector.entryExists(aliceDN));

		assertEquals(2L, MonitorRegistries.getMetricRegistry().getTimers().get("myCache.ldapStore.getTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get("myCache.ldapStore.searchTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get("myCache.ldapStore.addTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get("myCache.ldapStore.modifyTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get("myCache.ldapStore.deleteTimer").getCount());

		// Add new entry
		Collection<com.unboundid.ldap.sdk.Attribute> attrs = new LinkedList<>();
		attrs.add(new Attribute("objectClass", "inetOrgPerson"));
		attrs.add(new Attribute("uid", "alice"));
		attrs.add(new Attribute("cn", "Alice Adams"));
		attrs.add(new Attribute("sn", "Adams"));
		attrs.add(new Attribute("mail", "alice@wonderland.net"));
		attrs.add(new Attribute("mail", "hello@alice.name"));

		ReadOnlyEntry newEntry = new ReadOnlyEntry(aliceDN, attrs);

		assertTrue(ldapConnector.addEntry(newEntry));

		assertEquals(2L, MonitorRegistries.getMetricRegistry().getTimers().get("myCache.ldapStore.getTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get("myCache.ldapStore.searchTimer").getCount());
		assertEquals(1L, MonitorRegistries.getMetricRegistry().getTimers().get("myCache.ldapStore.addTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get("myCache.ldapStore.modifyTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get("myCache.ldapStore.deleteTimer").getCount());

		// Verify addition
		Entry entry = getTestLDAPServer().getEntry(aliceDN.toString());
		assertEquals(aliceDN.toString(), entry.getDN());
		assertTrue(Arrays.asList(entry.getAttribute("objectClass").getValues()).contains("top"));
		assertTrue(Arrays.asList(entry.getAttribute("objectClass").getValues()).contains("person"));
		assertTrue(Arrays.asList(entry.getAttribute("objectClass").getValues()).contains("organizationalPerson"));
		assertTrue(Arrays.asList(entry.getAttribute("objectClass").getValues()).contains("inetOrgPerson"));
		assertEquals(4, entry.getAttribute("objectClass").size());
		assertEquals("alice", entry.getAttributeValue("uid"));
		assertEquals("Alice Adams", entry.getAttributeValue("cn"));
		assertEquals("Adams", entry.getAttributeValue("sn"));
		assertTrue(Arrays.asList(entry.getAttribute("mail").getValues()).contains("alice@wonderland.net"));
		assertTrue(Arrays.asList(entry.getAttribute("mail").getValues()).contains("hello@alice.name"));
		assertEquals(2, entry.getAttribute("mail").size());
		assertEquals(5, entry.getAttributes().size());

		// Try to add again for same DN
		assertFalse(ldapConnector.addEntry(newEntry));

		assertEquals(2L, MonitorRegistries.getMetricRegistry().getTimers().get("myCache.ldapStore.getTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get("myCache.ldapStore.searchTimer").getCount());
		assertEquals(2L, MonitorRegistries.getMetricRegistry().getTimers().get("myCache.ldapStore.addTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get("myCache.ldapStore.modifyTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get("myCache.ldapStore.deleteTimer").getCount());

		// Test replace
		attrs = new LinkedList<>();
		attrs.add(new Attribute("objectClass", "inetOrgPerson"));
		attrs.add(new Attribute("uid", "alice"));
		attrs.add(new Attribute("cn", "Alice Brown"));
		attrs.add(new Attribute("sn", "Brown"));
		attrs.add(new Attribute("mail", "alice@myland.net"));
		attrs.add(new Attribute("mail", "hello@alice.name"));
		ReadOnlyEntry updatedEntry = new ReadOnlyEntry(aliceDN, attrs);

		assertTrue(ldapConnector.replaceEntry(updatedEntry));

		assertEquals(2L, MonitorRegistries.getMetricRegistry().getTimers().get("myCache.ldapStore.getTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get("myCache.ldapStore.searchTimer").getCount());
		assertEquals(2L, MonitorRegistries.getMetricRegistry().getTimers().get("myCache.ldapStore.addTimer").getCount());
		assertEquals(1L, MonitorRegistries.getMetricRegistry().getTimers().get("myCache.ldapStore.modifyTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get("myCache.ldapStore.deleteTimer").getCount());

		// Verify replace
		entry = getTestLDAPServer().getEntry(aliceDN.toString());
		assertEquals(aliceDN.toString(), entry.getDN());
		assertTrue(Arrays.asList(entry.getAttribute("objectClass").getValues()).contains("top"));
		assertTrue(Arrays.asList(entry.getAttribute("objectClass").getValues()).contains("person"));
		assertTrue(Arrays.asList(entry.getAttribute("objectClass").getValues()).contains("organizationalPerson"));
		assertTrue(Arrays.asList(entry.getAttribute("objectClass").getValues()).contains("inetOrgPerson"));
		assertEquals(4, entry.getAttribute("objectClass").size());
		assertEquals("alice", entry.getAttributeValue("uid"));
		assertEquals(1, entry.getAttribute("uid").size());
		assertEquals("Alice Brown", entry.getAttributeValue("cn"));
		assertEquals(1, entry.getAttribute("cn").size());
		assertEquals("Brown", entry.getAttributeValue("sn"));
		assertEquals(1, entry.getAttribute("sn").size());
		assertTrue(Arrays.asList(entry.getAttribute("mail").getValues()).contains("alice@myland.net"));
		assertTrue(Arrays.asList(entry.getAttribute("mail").getValues()).contains("hello@alice.name"));
		assertEquals(2, entry.getAttribute("mail").size());
		assertEquals(5, entry.getAttributes().size());

		// Test delete
		assertTrue(ldapConnector.deleteEntry(aliceDN));

		assertEquals(2L, MonitorRegistries.getMetricRegistry().getTimers().get("myCache.ldapStore.getTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get("myCache.ldapStore.searchTimer").getCount());
		assertEquals(2L, MonitorRegistries.getMetricRegistry().getTimers().get("myCache.ldapStore.addTimer").getCount());
		assertEquals(1L, MonitorRegistries.getMetricRegistry().getTimers().get("myCache.ldapStore.modifyTimer").getCount());
		assertEquals(1L, MonitorRegistries.getMetricRegistry().getTimers().get("myCache.ldapStore.deleteTimer").getCount());

		// Verify deletion
		assertNull(getTestLDAPServer().getEntry(aliceDN.toString()));

		// Try to delete non existing entry
		assertFalse(ldapConnector.deleteEntry(aliceDN));

		// Try to replace non existing entry
		assertFalse(ldapConnector.replaceEntry(updatedEntry));
	}


	public void testRunWithLanguageTags()
		throws Exception {

		LDAPStoreConfiguration config = new LDAPStoreConfiguration(getTestProperties());

		LDAPConnector ldapConnector = new LDAPConnector(config, CACHE_NAME, ATTRIBUTES, true);

		// Some DN
		DN aliceDN = new DN("uid=alice,ou=people,dc=wonderland,dc=net");

		// Add new entry
		Collection<com.unboundid.ldap.sdk.Attribute> attrs = new LinkedList<>();
		attrs.add(new Attribute("objectClass", "inetOrgPerson"));
		attrs.add(new Attribute("uid", "alice"));
		attrs.add(new Attribute("cn", "Alice Adams"));
		attrs.add(new Attribute("sn", "Adams"));
		attrs.add(new Attribute("l;lang-en", "Cologne"));
		attrs.add(new Attribute("l;lang-de", "Köln"));

		ReadOnlyEntry newEntry = new ReadOnlyEntry(aliceDN, attrs);

		assertTrue(ldapConnector.addEntry(newEntry));

		// Verify addition
		Entry entry = getTestLDAPServer().getEntry(aliceDN.toString());
		assertEquals(aliceDN.toString(), entry.getDN());
		assertTrue(Arrays.asList(entry.getAttribute("objectClass").getValues()).contains("top"));
		assertTrue(Arrays.asList(entry.getAttribute("objectClass").getValues()).contains("person"));
		assertTrue(Arrays.asList(entry.getAttribute("objectClass").getValues()).contains("organizationalPerson"));
		assertTrue(Arrays.asList(entry.getAttribute("objectClass").getValues()).contains("inetOrgPerson"));
		assertEquals(4, entry.getAttribute("objectClass").size());
		assertEquals("alice", entry.getAttributeValue("uid"));
		assertEquals(1, entry.getAttribute("uid").size());
		assertEquals("Alice Adams", entry.getAttributeValue("cn"));
		assertEquals(1, entry.getAttribute("cn").size());
		assertEquals("Adams", entry.getAttributeValue("sn"));
		assertEquals(1, entry.getAttribute("sn").size());
		assertEquals("Cologne", entry.getAttribute("l;lang-en").getValue());
		assertEquals(1, entry.getAttribute("l;lang-en").size());
		assertEquals("Köln", entry.getAttribute("l;lang-de").getValue());
		assertEquals(1, entry.getAttribute("l;lang-de").size());
		assertNull(entry.getAttribute("l"));
		assertEquals(10, entry.toLDIF().length);

		// Replace language tagged attribute with untagged attribute
		attrs = new LinkedList<>();
		attrs.add(new Attribute("objectClass", "inetOrgPerson"));
		attrs.add(new Attribute("uid", "alice"));
		attrs.add(new Attribute("cn", "Alice Adams"));
		attrs.add(new Attribute("sn", "Adams"));
		attrs.add(new Attribute("l", "London"));

		ReadOnlyEntry updatedEntry = new ReadOnlyEntry(aliceDN, attrs);
		assertTrue(ldapConnector.replaceEntry(updatedEntry));

		entry = getTestLDAPServer().getEntry(aliceDN.toString());
		assertEquals(aliceDN.toString(), entry.getDN());
		assertTrue(Arrays.asList(entry.getAttribute("objectClass").getValues()).contains("top"));
		assertTrue(Arrays.asList(entry.getAttribute("objectClass").getValues()).contains("person"));
		assertTrue(Arrays.asList(entry.getAttribute("objectClass").getValues()).contains("organizationalPerson"));
		assertTrue(Arrays.asList(entry.getAttribute("objectClass").getValues()).contains("inetOrgPerson"));
		assertEquals(4, entry.getAttribute("objectClass").size());
		assertEquals("alice", entry.getAttributeValue("uid"));
		assertEquals(1, entry.getAttribute("uid").size());
		assertEquals("Alice Adams", entry.getAttributeValue("cn"));
		assertEquals(1, entry.getAttribute("cn").size());
		assertEquals("Adams", entry.getAttributeValue("sn"));
		assertEquals(1, entry.getAttribute("sn").size());
		assertEquals("London", entry.getAttribute("l").getValue());
		assertEquals(1, entry.getAttribute("l").size());
		assertNull(entry.getAttribute("l;lang-en"));
		assertNull(entry.getAttribute("l;lang-de"));
		assertEquals(9, entry.toLDIF().length);

		// Replace plain attribute with language tagged
		attrs = new LinkedList<>();
		attrs.add(new Attribute("objectClass", "inetOrgPerson"));
		attrs.add(new Attribute("uid", "alice"));
		attrs.add(new Attribute("cn", "Alice Adams"));
		attrs.add(new Attribute("sn", "Adams"));
		attrs.add(new Attribute("l;lang-en", "Cologne"));
		attrs.add(new Attribute("l;lang-de", "Köln"));

		updatedEntry = new ReadOnlyEntry(aliceDN, attrs);

		assertTrue(ldapConnector.replaceEntry(updatedEntry));

		entry = getTestLDAPServer().getEntry(aliceDN.toString());
		assertEquals(aliceDN.toString(), entry.getDN());
		assertTrue(Arrays.asList(entry.getAttribute("objectClass").getValues()).contains("top"));
		assertTrue(Arrays.asList(entry.getAttribute("objectClass").getValues()).contains("person"));
		assertTrue(Arrays.asList(entry.getAttribute("objectClass").getValues()).contains("organizationalPerson"));
		assertTrue(Arrays.asList(entry.getAttribute("objectClass").getValues()).contains("inetOrgPerson"));
		assertEquals(4, entry.getAttribute("objectClass").size());
		assertEquals("alice", entry.getAttributeValue("uid"));
		assertEquals(1, entry.getAttribute("uid").size());
		assertEquals("Alice Adams", entry.getAttributeValue("cn"));
		assertEquals(1, entry.getAttribute("cn").size());
		assertEquals("Adams", entry.getAttributeValue("sn"));
		assertEquals(1, entry.getAttribute("sn").size());
		assertEquals("Cologne", entry.getAttribute("l;lang-en").getValue());
		assertEquals(1, entry.getAttribute("l;lang-en").size());
		assertEquals("Köln", entry.getAttribute("l;lang-de").getValue());
		assertEquals(1, entry.getAttribute("l;lang-de").size());
		assertNull(entry.getAttribute("l"));
		assertEquals(10, entry.toLDIF().length);

		// Replace again with identical entry (to test no-diff handling)
		assertTrue(ldapConnector.replaceEntry(updatedEntry));

		entry = getTestLDAPServer().getEntry(aliceDN.toString());
		assertEquals(aliceDN.toString(), entry.getDN());
		assertTrue(Arrays.asList(entry.getAttribute("objectClass").getValues()).contains("top"));
		assertTrue(Arrays.asList(entry.getAttribute("objectClass").getValues()).contains("person"));
		assertTrue(Arrays.asList(entry.getAttribute("objectClass").getValues()).contains("organizationalPerson"));
		assertTrue(Arrays.asList(entry.getAttribute("objectClass").getValues()).contains("inetOrgPerson"));
		assertEquals(4, entry.getAttribute("objectClass").size());
		assertEquals("alice", entry.getAttributeValue("uid"));
		assertEquals(1, entry.getAttribute("uid").size());
		assertEquals("Alice Adams", entry.getAttributeValue("cn"));
		assertEquals(1, entry.getAttribute("cn").size());
		assertEquals("Adams", entry.getAttributeValue("sn"));
		assertEquals(1, entry.getAttribute("sn").size());
		assertEquals("Cologne", entry.getAttribute("l;lang-en").getValue());
		assertEquals(1, entry.getAttribute("l;lang-en").size());
		assertEquals("Köln", entry.getAttribute("l;lang-de").getValue());
		assertEquals(1, entry.getAttribute("l;lang-de").size());
		assertNull(entry.getAttribute("l"));
		assertEquals(10, entry.toLDIF().length);

		// Test delete
		assertTrue(ldapConnector.deleteEntry(aliceDN));

		// Verify deletion
		assertNull(getTestLDAPServer().getEntry(aliceDN.toString()));

		// Try to delete non existing entry
		assertFalse(ldapConnector.deleteEntry(aliceDN));

		// Try to replace non existing entry
		assertFalse(ldapConnector.replaceEntry(updatedEntry));
	}


	public void testAddWithServerDown()
		throws Exception {

		LDAPStoreConfiguration config = new LDAPStoreConfiguration(getTestProperties());

		LDAPConnector ldapConnector = new LDAPConnector(config, CACHE_NAME, ATTRIBUTES, false);

		getTestLDAPServer().shutDown(true);

		Thread.sleep(50);

		DN aliceDN = new DN("uid=alice,ou=people,dc=wonderland,dc=net");

		Collection<com.unboundid.ldap.sdk.Attribute> attrs = new LinkedList<>();
		attrs.add(new Attribute("objectClass", "inetOrgPerson"));
		attrs.add(new Attribute("uid", "alice"));
		attrs.add(new Attribute("cn", "Alice Adams"));
		attrs.add(new Attribute("sn", "Adams"));
		attrs.add(new Attribute("mail", "alice@wonderland.net"));
		attrs.add(new Attribute("mail", "hello@alice.name"));

		ReadOnlyEntry newEntry = new ReadOnlyEntry(aliceDN, attrs);

		try {
			ldapConnector.addEntry(newEntry);
			fail();
		} catch (PersistenceException e) {
			assertEquals("LDAP add for uid=alice,ou=people,dc=wonderland,dc=net failed: result code='91 (connect error)'", e.getMessage());
		}
	}


	public void testRetrieveWithServerDown()
		throws Exception {

		LDAPStoreConfiguration config = new LDAPStoreConfiguration(getTestProperties());

		LDAPConnector ldapConnector = new LDAPConnector(config, CACHE_NAME, ATTRIBUTES, false);

		getTestLDAPServer().shutDown(true);

		Thread.sleep(50);

		DN aliceDN = new DN("uid=alice,ou=people,dc=wonderland,dc=net");

		try {
			ldapConnector.retrieveEntry(aliceDN);
			fail();
		} catch (PersistenceException e) {
			assertEquals("LDAP get of uid=alice,ou=people,dc=wonderland,dc=net failed: result code='91 (connect error)'", e.getMessage());
		}
	}


	public void testEntryExistsWithServerDown()
		throws Exception {

		LDAPStoreConfiguration config = new LDAPStoreConfiguration(getTestProperties());

		LDAPConnector ldapConnector = new LDAPConnector(config, CACHE_NAME, ATTRIBUTES, false);

		getTestLDAPServer().shutDown(true);

		Thread.sleep(50);

		DN aliceDN = new DN("uid=alice,ou=people,dc=wonderland,dc=net");

		try {
			ldapConnector.entryExists(aliceDN);
			fail();
		} catch (PersistenceException e) {
			assertEquals("LDAP get of uid=alice,ou=people,dc=wonderland,dc=net failed: result code='91 (connect error)'", e.getMessage());
		}
	}


	public void testReplaceWithServerDown()
		throws Exception {

		LDAPStoreConfiguration config = new LDAPStoreConfiguration(getTestProperties());

		LDAPConnector ldapConnector = new LDAPConnector(config, CACHE_NAME, ATTRIBUTES, false);

		getTestLDAPServer().shutDown(true);

		Thread.sleep(50);

		DN aliceDN = new DN("uid=alice,ou=people,dc=wonderland,dc=net");

		Collection<com.unboundid.ldap.sdk.Attribute> attrs = new LinkedList<>();
		attrs.add(new Attribute("objectClass", "inetOrgPerson"));
		attrs.add(new Attribute("uid", "alice"));
		attrs.add(new Attribute("cn", "Alice Adams"));
		attrs.add(new Attribute("sn", "Adams"));
		attrs.add(new Attribute("mail", "alice@wonderland.net"));
		attrs.add(new Attribute("mail", "hello@alice.name"));

		ReadOnlyEntry newEntry = new ReadOnlyEntry(aliceDN, attrs);

		try {
			ldapConnector.replaceEntry(newEntry);
			fail();
		} catch (PersistenceException e) {
			assertEquals("LDAP modify for uid=alice,ou=people,dc=wonderland,dc=net failed: result code='91 (connect error)'", e.getMessage());
		}
	}


	public void testDeleteEntryWithServerDown()
		throws Exception {

		LDAPStoreConfiguration config = new LDAPStoreConfiguration(getTestProperties());

		LDAPConnector ldapConnector = new LDAPConnector(config, CACHE_NAME, ATTRIBUTES, false);

		getTestLDAPServer().shutDown(true);

		Thread.sleep(50);

		DN aliceDN = new DN("uid=alice,ou=people,dc=wonderland,dc=net");

		try {
			ldapConnector.deleteEntry(aliceDN);
			fail();
		} catch (PersistenceException e) {
			assertEquals("LDAP delete of uid=alice,ou=people,dc=wonderland,dc=net failed: result code='91 (connect error)'", e.getMessage());
		}
	}


	// Test that LDAP get returns all attributes with options for a base attribute name
	public void testGetWithOptions()
		throws Exception {

		// Some DN
		DN aliceDN = new DN("uid=alice,ou=people,dc=wonderland,dc=net");

		// Add new entry
		Collection<com.unboundid.ldap.sdk.Attribute> attrs = new LinkedList<>();
		attrs.add(new Attribute("objectClass", "inetOrgPerson"));
		attrs.add(new Attribute("uid", "alice"));
		attrs.add(new Attribute("cn", "Alice Adams"));
		attrs.add(new Attribute("sn", "Adams"));
		attrs.add(new Attribute("l;lang-fr", "Cologne"));
		attrs.add(new Attribute("l;lang-de", "Köln"));

		Entry newEntry = new Entry(aliceDN, attrs);

		getTestLDAPServer().add(newEntry);

		Entry entry = getTestLDAPServer().getEntry(aliceDN.toString(), "l");

		assertEquals("Cologne", entry.getAttributeValue("l;lang-fr"));
		assertEquals("Köln", entry.getAttributeValue("l;lang-de"));
	}


	public void testAddCountDelete()
		throws Exception {

		LDAPStoreConfiguration config = new LDAPStoreConfiguration(getTestProperties());

		LDAPConnector ldapConnector = new LDAPConnector(config, CACHE_NAME, ATTRIBUTES, false);

		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get("myCache.ldapStore.getTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get("myCache.ldapStore.searchTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get("myCache.ldapStore.addTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get("myCache.ldapStore.modifyTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get("myCache.ldapStore.deleteTimer").getCount());

		assertEquals(0, ldapConnector.countEntries());

		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get("myCache.ldapStore.getTimer").getCount());
		assertEquals(1L, MonitorRegistries.getMetricRegistry().getTimers().get("myCache.ldapStore.searchTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get("myCache.ldapStore.addTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get("myCache.ldapStore.modifyTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get("myCache.ldapStore.deleteTimer").getCount());

		final int COUNT = 2000;

		Collection<com.unboundid.ldap.sdk.Attribute> attrs = new LinkedList<>();
		attrs.add(new Attribute("objectClass", "inetOrgPerson"));
		attrs.add(new Attribute("cn", "Alice Adams"));
		attrs.add(new Attribute("sn", "Adams"));

		// Add 2000 entries
		for (int i=0; i < COUNT; ++i) {
			DN dn = new DN("uid=alice-" + i + ", " + config.ldapDirectory.baseDN);
			ReadOnlyEntry entry = new ReadOnlyEntry(dn, attrs);
			ldapConnector.addEntry(entry);
		}

		// 3 pages
		assertEquals(COUNT, ldapConnector.countEntries());

		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get("myCache.ldapStore.getTimer").getCount());
		assertEquals(5L, MonitorRegistries.getMetricRegistry().getTimers().get("myCache.ldapStore.searchTimer").getCount());
		assertEquals(COUNT, MonitorRegistries.getMetricRegistry().getTimers().get("myCache.ldapStore.addTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get("myCache.ldapStore.modifyTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get("myCache.ldapStore.deleteTimer").getCount());

		// Delete all
		assertEquals(COUNT, ldapConnector.deleteEntries());

		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get("myCache.ldapStore.getTimer").getCount());
		assertEquals(9L, MonitorRegistries.getMetricRegistry().getTimers().get("myCache.ldapStore.searchTimer").getCount());
		assertEquals(COUNT, MonitorRegistries.getMetricRegistry().getTimers().get("myCache.ldapStore.addTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get("myCache.ldapStore.modifyTimer").getCount());
		assertEquals(COUNT, MonitorRegistries.getMetricRegistry().getTimers().get("myCache.ldapStore.deleteTimer").getCount());
	}


	// https://bitbucket.org/connect2id/authorization-store/issues/109/multi-threaded-ldap-search-with-cookie
	public void testSearchWithCookieKeepsConnectionUntilComplete()
		throws Exception {

		Properties props = getTestProperties();
		props.setProperty("ldapDirectory.pageSize", "10"); // reduce page size to ten

		LDAPStoreConfiguration config = new LDAPStoreConfiguration(props);

		assertEquals(10, config.ldapDirectory.pageSize);

		LDAPConnector ldapConnector = new LDAPConnector(config, CACHE_NAME, ATTRIBUTES, false);

		// Create 100 records
		Collection<com.unboundid.ldap.sdk.Attribute> attrs = new LinkedList<>();
		attrs.add(new Attribute("objectClass", "inetOrgPerson"));
		attrs.add(new Attribute("cn", "Alice Adams"));
		attrs.add(new Attribute("sn", "Adams"));

		// Add 2000 entries
		for (int i=0; i < 100; ++i) {
			DN dn = new DN("uid=alice-" + i + ", " + config.ldapDirectory.baseDN);
			Entry entry = new Entry(dn, attrs);
			getTestLDAPServer().add(entry);
		}

		final long numCheckouts = ldapConnector.getPool().getConnectionPoolStatistics().getNumSuccessfulCheckouts();

		ldapConnector.countEntries();

		assertEquals(numCheckouts + 1, ldapConnector.getPool().getConnectionPoolStatistics().getNumSuccessfulCheckouts());
	}
}
