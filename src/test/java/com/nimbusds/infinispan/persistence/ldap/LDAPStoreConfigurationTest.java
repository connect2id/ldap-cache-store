package com.nimbusds.infinispan.persistence.ldap;


import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import com.unboundid.ldap.sdk.DN;
import com.unboundid.ldap.sdk.LDAPURL;
import junit.framework.TestCase;


public class LDAPStoreConfigurationTest extends TestCase {
	

	public static Properties getTestProperties() {

		Properties props = new Properties();

		try {
			props.load(new FileInputStream("test.properties"));
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		return props;
	}


	public void testParseTestProps()
		throws Exception {

		LDAPStoreConfiguration config = new LDAPStoreConfiguration(getTestProperties());

		// Infinispan
		assertFalse(config.purgeOnStartup());
		assertFalse(config.fetchPersistentState());
		assertFalse(config.ignoreModifications());
		assertNull(config.async());
		assertNull(config.singletonStore());
		assertFalse(config.preload());
		assertTrue(config.shared());

		// LDAP stuff

		assertEquals(new DN("ou=people,dc=wonderland,dc=net"), config.ldapDirectory.baseDN);
		assertEquals(500, config.ldapDirectory.pageSize);
		assertEquals("com.nimbusds.infinispan.persistence.ldap.UserEntryTransformer", config.ldapDirectory.entryTransformer);
		assertEquals("com.nimbusds.infinispan.persistence.ldap.UserQueryExecutor", config.ldapDirectory.queryExecutor);

		assertEquals(new DN("cn=Directory Manager"), config.ldapUser.dn);
		assertEquals("secret", config.ldapUser.password);

		assertEquals(new LDAPURL("ldap://127.0.0.1:30389"), config.ldapServer.url[0]);
		assertEquals(1, config.ldapServer.url.length);
		assertNull(config.ldapServer.selectionAlgorithm);
		assertEquals(500, config.ldapServer.connectTimeout);
		assertEquals(500, config.ldapServer.responseTimeout);
		assertEquals(com.nimbusds.common.ldap.LDAPConnectionSecurity.NONE, config.ldapServer.security);
		assertFalse(config.ldapServer.trustSelfSignedCerts);
		assertEquals(10, config.ldapServer.connectionPoolSize);
		assertEquals(0, config.ldapServer.connectionPoolInitialSize);
		assertEquals(250, config.ldapServer.connectionPoolMaxWaitTime);
	}


	public void testUnspecifiedQueryExecutor()
		throws Exception {

		Properties testProps = getTestProperties();
		testProps.remove("ldapDirectory.queryExecutor");
		
		LDAPStoreConfiguration config = new LDAPStoreConfiguration(testProps);

		// Infinispan
		assertFalse(config.purgeOnStartup());
		assertFalse(config.fetchPersistentState());
		assertFalse(config.ignoreModifications());
		assertNull(config.async());
		assertNull(config.singletonStore());
		assertFalse(config.preload());
		assertTrue(config.shared());

		// LDAP stuff

		assertEquals(new DN("ou=people,dc=wonderland,dc=net"), config.ldapDirectory.baseDN);
		assertEquals(500, config.ldapDirectory.pageSize);
		assertEquals("com.nimbusds.infinispan.persistence.ldap.UserEntryTransformer", config.ldapDirectory.entryTransformer);
		assertNull(config.ldapDirectory.queryExecutor);

		assertEquals(new DN("cn=Directory Manager"), config.ldapUser.dn);
		assertEquals("secret", config.ldapUser.password);

		assertEquals(new LDAPURL("ldap://127.0.0.1:30389"), config.ldapServer.url[0]);
		assertEquals(1, config.ldapServer.url.length);
		assertNull(config.ldapServer.selectionAlgorithm);
		assertEquals(500, config.ldapServer.connectTimeout);
		assertEquals(500, config.ldapServer.responseTimeout);
		assertEquals(com.nimbusds.common.ldap.LDAPConnectionSecurity.NONE, config.ldapServer.security);
		assertFalse(config.ldapServer.trustSelfSignedCerts);
		assertEquals(10, config.ldapServer.connectionPoolSize);
		assertEquals(0, config.ldapServer.connectionPoolInitialSize);
		assertEquals(250, config.ldapServer.connectionPoolMaxWaitTime);
	}
	
	
	public void testEmptyQueryExecutor()
		throws Exception {

		Properties testProps = getTestProperties();
		testProps.setProperty("ldapDirectory.queryExecutor", "");
		
		LDAPStoreConfiguration config = new LDAPStoreConfiguration(testProps);

		// Infinispan
		assertFalse(config.purgeOnStartup());
		assertFalse(config.fetchPersistentState());
		assertFalse(config.ignoreModifications());
		assertNull(config.async());
		assertNull(config.singletonStore());
		assertFalse(config.preload());
		assertTrue(config.shared());

		// LDAP stuff

		assertEquals(new DN("ou=people,dc=wonderland,dc=net"), config.ldapDirectory.baseDN);
		assertEquals(500, config.ldapDirectory.pageSize);
		assertEquals("com.nimbusds.infinispan.persistence.ldap.UserEntryTransformer", config.ldapDirectory.entryTransformer);
		assertNull(config.ldapDirectory.queryExecutor);

		assertEquals(new DN("cn=Directory Manager"), config.ldapUser.dn);
		assertEquals("secret", config.ldapUser.password);

		assertEquals(new LDAPURL("ldap://127.0.0.1:30389"), config.ldapServer.url[0]);
		assertEquals(1, config.ldapServer.url.length);
		assertNull(config.ldapServer.selectionAlgorithm);
		assertEquals(500, config.ldapServer.connectTimeout);
		assertEquals(500, config.ldapServer.responseTimeout);
		assertEquals(com.nimbusds.common.ldap.LDAPConnectionSecurity.NONE, config.ldapServer.security);
		assertFalse(config.ldapServer.trustSelfSignedCerts);
		assertEquals(10, config.ldapServer.connectionPoolSize);
		assertEquals(0, config.ldapServer.connectionPoolInitialSize);
		assertEquals(250, config.ldapServer.connectionPoolMaxWaitTime);
	}
	
	
	public void testSystemPropertyInterpolation()
		throws Exception {
		
		Properties testProps = getTestProperties();
		testProps.setProperty("ldapUser.password", "${ldapUser.password}");
		
		System.setProperty("ldapUser.password", "noOneKnowsThis");
		
		LDAPStoreConfiguration config = new LDAPStoreConfiguration(testProps);
		
		assertEquals("noOneKnowsThis", config.ldapUser.password);
		
		System.clearProperty("ldapUser.password");
	}
}
