package com.nimbusds.infinispan.persistence.ldap.backend;


import java.util.*;
import java.util.stream.Collectors;

import com.unboundid.ldap.sdk.*;
import junit.framework.TestCase;
import org.junit.Assert;


public class LDAPModifyRequestFactoryTest extends TestCase {


	private static Set<String> getModifiableAttributes() {

		return Collections.unmodifiableSet(new HashSet<>(Arrays.asList(
			"givenName",
			"familyName",
			"age",
			"points",
			"active",
			"hosted",
			"mail")));
	}
	

	private static Entry composeEntry()
		throws LDAPException {

		DN dn = new DN("uid=alice,ou=people,dc=wonderland,dc=net");

		Collection<com.unboundid.ldap.sdk.Attribute> attrs = new LinkedList<>();
		attrs.add(new Attribute("objectClass", "top", "person"));
		attrs.add(new Attribute("uid", "alice"));
		attrs.add(new Attribute("givenName", "Alice"));
		attrs.add(new Attribute("familyName", "Adams"));
		attrs.add(new Attribute("age", "21"));
		attrs.add(new Attribute("points", "1000"));
		attrs.add(new Attribute("active", "TRUE"));
		attrs.add(new Attribute("hosted", "FALSE"));
		attrs.add(new Attribute("mail", "alice@wonderland.net", "hello@alice.name"));

		return new Entry(dn, attrs);
	}


	public void testEntry()
		throws Exception {

		Entry entry = composeEntry();

		assertEquals("uid=alice,ou=people,dc=wonderland,dc=net", entry.getDN());

		assertTrue(Arrays.asList(entry.getAttribute("objectClass").getValues()).contains("top"));
		assertTrue(Arrays.asList(entry.getAttribute("objectClass").getValues()).contains("person"));
		assertEquals(2, entry.getAttribute("objectClass").getValues().length);

		assertEquals("alice", entry.getAttributeValue("uid"));

		assertEquals("Alice", entry.getAttributeValue("givenName"));

		assertEquals("Adams", entry.getAttributeValue("familyName"));

		assertEquals(21, entry.getAttributeValueAsInteger("age").intValue());

		assertEquals(1000, entry.getAttributeValueAsInteger("points").intValue());

		assertTrue(entry.getAttributeValueAsBoolean("active"));

		assertFalse(entry.getAttributeValueAsBoolean("hosted"));

		assertEquals("alice@wonderland.net", entry.getAttribute("mail").getValues()[0]);
		assertEquals("hello@alice.name", entry.getAttribute("mail").getValues()[1]);
		assertEquals(2, entry.getAttribute("mail").getValues().length);

		assertEquals(9, entry.getAttributes().size());
	}


	public void testGetAttributes()
		throws Exception {

		Set<String> attrs = getModifiableAttributes();

		LDAPModifyRequestFactory f = new LDAPModifyRequestFactory(attrs);

		assertEquals(attrs, f.getModifiableAttributes());
	}


	public void testUpdateAllAttributes()
		throws Exception {

		LDAPModifyRequestFactory f = new LDAPModifyRequestFactory(getModifiableAttributes());

		Entry entry = composeEntry();

		ModifyRequest request = f.composeModifyRequest(entry);

		assertEquals("uid=alice,ou=people,dc=wonderland,dc=net", request.getDN());

		List<Modification> mods = request.getModifications();

		assertEquals(ModificationType.REPLACE, mods.get(0).getModificationType());
		assertEquals(ModificationType.REPLACE, mods.get(1).getModificationType());
		assertEquals(ModificationType.REPLACE, mods.get(2).getModificationType());
		assertEquals(ModificationType.REPLACE, mods.get(3).getModificationType());
		assertEquals(ModificationType.REPLACE, mods.get(4).getModificationType());
		assertEquals(ModificationType.REPLACE, mods.get(5).getModificationType());
		assertEquals(ModificationType.REPLACE, mods.get(6).getModificationType());

		assertEquals("givenName", mods.get(0).getAttributeName());
		assertEquals("Alice",     mods.get(0).getValues()[0]);
		assertEquals(1,           mods.get(0).getValues().length);

		assertEquals("familyName", mods.get(1).getAttributeName());
		assertEquals("Adams",      mods.get(1).getValues()[0]);
		assertEquals(1,            mods.get(1).getValues().length);

		assertEquals("age", mods.get(2).getAttributeName());
		assertEquals("21",  mods.get(2).getValues()[0]);
		assertEquals(1,     mods.get(2).getValues().length);

		assertEquals("points", mods.get(3).getAttributeName());
		assertEquals("1000",   mods.get(3).getValues()[0]);
		assertEquals(1,        mods.get(3).getValues().length);

		assertEquals("active", mods.get(4).getAttributeName());
		assertEquals("TRUE",   mods.get(4).getValues()[0]);
		assertEquals(1,        mods.get(4).getValues().length);

		assertEquals("hosted", mods.get(5).getAttributeName());
		assertEquals("FALSE",  mods.get(5).getValues()[0]);
		assertEquals(1,        mods.get(5).getValues().length);

		assertEquals("mail",                 mods.get(6).getAttributeName());
		assertEquals("alice@wonderland.net", mods.get(6).getValues()[0]);
		assertEquals("hello@alice.name",     mods.get(6).getValues()[1]);
		assertEquals(2,                      mods.get(6).getValues().length);

		assertEquals(7, mods.size());

		assertTrue(request.getControlList().isEmpty());
	}


	public void testDeleteAllAttributes()
		throws Exception {

		LDAPModifyRequestFactory f = new LDAPModifyRequestFactory(getModifiableAttributes());

		Entry entry = composeEntry();

		entry.removeAttribute("givenName");
		entry.removeAttribute("familyName");
		entry.removeAttribute("age");
		entry.removeAttribute("points");
		entry.removeAttribute("active");
		entry.removeAttribute("hosted");
		entry.removeAttribute("mail");

		ModifyRequest request = f.composeModifyRequest(entry);

		assertEquals("uid=alice,ou=people,dc=wonderland,dc=net", request.getDN());

		List<Modification> mods = request.getModifications();

		assertEquals(ModificationType.REPLACE, mods.get(0).getModificationType());
		assertEquals(ModificationType.REPLACE, mods.get(1).getModificationType());
		assertEquals(ModificationType.REPLACE, mods.get(2).getModificationType());
		assertEquals(ModificationType.REPLACE, mods.get(3).getModificationType());
		assertEquals(ModificationType.REPLACE, mods.get(4).getModificationType());
		assertEquals(ModificationType.REPLACE, mods.get(5).getModificationType());
		assertEquals(ModificationType.REPLACE, mods.get(6).getModificationType());

		List<String> names = mods.stream().map(Modification::getAttributeName).collect(Collectors.toList());

		assertTrue(names.contains("givenName"));
		assertTrue(names.contains("familyName"));
		assertTrue(names.contains("age"));
		assertTrue(names.contains("points"));
		assertTrue(names.contains("active"));
		assertTrue(names.contains("hosted"));
		assertTrue(names.contains("mail"));
		assertEquals(7, names.size());

		for (Modification m: mods) {
			assertEquals(0, m.getAttribute().size());
		}

		assertEquals(7, mods.size());

		assertTrue(request.getControlList().isEmpty());
	}


	public void testMixed()
		throws Exception {

		LDAPModifyRequestFactory f = new LDAPModifyRequestFactory(getModifiableAttributes());

		Entry entry = composeEntry();

		entry.removeAttribute("points");
		entry.removeAttribute("active");

		ModifyRequest request = f.composeModifyRequest(entry);

		assertEquals("uid=alice,ou=people,dc=wonderland,dc=net", request.getDN());

		List<Modification> mods = request.getModifications();

		assertEquals(ModificationType.REPLACE, mods.get(0).getModificationType());
		assertEquals(ModificationType.REPLACE, mods.get(1).getModificationType());
		assertEquals(ModificationType.REPLACE, mods.get(2).getModificationType());
		assertEquals(ModificationType.REPLACE, mods.get(3).getModificationType());
		assertEquals(ModificationType.REPLACE, mods.get(4).getModificationType());
		assertEquals(ModificationType.REPLACE, mods.get(5).getModificationType());
		assertEquals(ModificationType.REPLACE, mods.get(6).getModificationType());

		assertEquals("givenName", mods.get(0).getAttributeName());
		assertEquals("Alice",     mods.get(0).getValues()[0]);
		assertEquals(1,           mods.get(0).getValues().length);

		assertEquals("familyName", mods.get(1).getAttributeName());
		assertEquals("Adams",      mods.get(1).getValues()[0]);
		assertEquals(1,            mods.get(1).getValues().length);

		assertEquals("age", mods.get(2).getAttributeName());
		assertEquals("21",  mods.get(2).getValues()[0]);
		assertEquals(1,     mods.get(2).getValues().length);

		assertEquals("hosted", mods.get(3).getAttributeName());
		assertEquals("FALSE",  mods.get(3).getValues()[0]);
		assertEquals(1,        mods.get(3).getValues().length);

		assertEquals("mail",                 mods.get(4).getAttributeName());
		assertEquals("alice@wonderland.net", mods.get(4).getValues()[0]);
		assertEquals("hello@alice.name",     mods.get(4).getValues()[1]);
		assertEquals(2,                      mods.get(4).getValues().length);

		assertEquals("active", mods.get(5).getAttributeName());
		assertEquals(0,        mods.get(5).getValues().length);

		assertEquals("points", mods.get(6).getAttributeName());
		assertEquals(0,        mods.get(6).getValues().length);

		assertEquals(7, mods.size());

		assertTrue(request.getControlList().isEmpty());
	}


	public void testUpdateWithLanguageTags()
		throws Exception {

		LDAPModifyRequestFactory f = new LDAPModifyRequestFactory(getModifiableAttributes());

		Entry entry = composeEntry();

		entry.removeAttribute("mail");
		entry.setAttribute(new Attribute("mail;lang-en", "hello@alice.com"));
		entry.setAttribute(new Attribute("mail;lang-fr", "hello@alice.fr"));

		System.out.println(entry.toLDIFString());

		ModifyRequest request = f.composeModifyRequest(entry);

		System.out.println(request.toLDIFString());

		List<Modification> mods = request.getModifications();

		assertEquals(ModificationType.REPLACE, mods.get(0).getModificationType());
		assertEquals(ModificationType.REPLACE, mods.get(1).getModificationType());
		assertEquals(ModificationType.REPLACE, mods.get(2).getModificationType());
		assertEquals(ModificationType.REPLACE, mods.get(3).getModificationType());
		assertEquals(ModificationType.REPLACE, mods.get(4).getModificationType());
		assertEquals(ModificationType.REPLACE, mods.get(5).getModificationType());
		assertEquals(ModificationType.REPLACE, mods.get(6).getModificationType());

		assertEquals("givenName", mods.get(0).getAttributeName());
		assertEquals("Alice",     mods.get(0).getValues()[0]);
		assertEquals(1,           mods.get(0).getValues().length);

		assertEquals("familyName", mods.get(1).getAttributeName());
		assertEquals("Adams",      mods.get(1).getValues()[0]);
		assertEquals(1,            mods.get(1).getValues().length);

		assertEquals("age", mods.get(2).getAttributeName());
		assertEquals("21",  mods.get(2).getValues()[0]);
		assertEquals(1,     mods.get(2).getValues().length);

		assertEquals("points", mods.get(3).getAttributeName());
		assertEquals("1000",   mods.get(3).getValues()[0]);
		assertEquals(1,        mods.get(3).getValues().length);

		assertEquals("active", mods.get(4).getAttributeName());
		assertEquals("TRUE",   mods.get(4).getValues()[0]);
		assertEquals(1,        mods.get(4).getValues().length);

		assertEquals("hosted", mods.get(5).getAttributeName());
		assertEquals("FALSE",  mods.get(5).getValues()[0]);
		assertEquals(1,        mods.get(5).getValues().length);

		assertEquals("mail",            mods.get(6).getAttribute().getBaseName());
		assertTrue(                     mods.get(6).getAttribute().getOptions().contains("lang-en"));
		assertEquals(1,                 mods.get(6).getAttribute().getOptions().size());
		assertEquals("hello@alice.com", mods.get(6).getValues()[0]);
		assertEquals(1,                 mods.get(6).getValues().length);

		assertEquals("mail",           mods.get(7).getAttribute().getBaseName());
		assertTrue(                    mods.get(7).getAttribute().getOptions().contains("lang-fr"));
		assertEquals(1,                mods.get(7).getAttribute().getOptions().size());
		assertEquals("hello@alice.fr", mods.get(7).getValues()[0]);
		assertEquals(1,                mods.get(7).getValues().length);

		assertEquals(8, mods.size());

		assertTrue(request.getControlList().isEmpty());
	}


	public void testUpdateWithBinary()
		throws Exception {

		Set<String> modifiableAttribues = Collections.singleton("jpegPhoto");

		LDAPModifyRequestFactory f = new LDAPModifyRequestFactory(modifiableAttribues);

		assertEquals(modifiableAttribues, f.getModifiableAttributes());

		Entry entry = composeEntry();

		byte[] jpegPhoto = {0,1,2,3};
		String jpegPhotoB64 = Base64.getEncoder().encodeToString(jpegPhoto);

		entry.addAttribute(new Attribute("jpegPhoto", jpegPhoto));

		Assert.assertArrayEquals(jpegPhoto, entry.getAttribute("jpegPhoto").getValueByteArray());

		ModifyRequest request = f.composeModifyRequest(entry);

		assertEquals(ModificationType.REPLACE, request.getModifications().get(0).getModificationType());
		assertEquals("jpegPhoto", request.getModifications().get(0).getAttributeName());
		Assert.assertArrayEquals(jpegPhoto, request.getModifications().get(0).getValueByteArrays()[0]);
		assertEquals(1, request.getModifications().size());
	}


	public void testModifyBasedOnDiff()
		throws Exception {

		LDAPModifyRequestFactory f = new LDAPModifyRequestFactory(getModifiableAttributes());

		Entry newEntry = composeEntry();
		newEntry.setAttribute("givenName;lang-en", "Alice");

		Entry oldEntry = composeEntry();
		oldEntry.setAttribute("points", "359");
		oldEntry.setAttribute("givenName;lang-es", "Maria");
		oldEntry.setAttribute("givenName;lang-en", "Mary");
		oldEntry.setAttribute("unlistedAttribute", "abc");

		ModifyRequest request = f.composeModifyRequest(newEntry, oldEntry);

		List<Modification> mods = request.getModifications();

		for (Modification m: mods) {

			if (ModificationType.DELETE.equals(m.getModificationType())) {

				switch (m.getAttribute().getName()) {

					case "points":
						assertEquals("points", m.getAttribute().getBaseName());
						assertEquals("359", m.getValues()[0]);
						break;
					case "givenName;lang-es":
						assertEquals("givenName", m.getAttribute().getBaseName());
						assertEquals("Maria", m.getValues()[0]);
						break;
					case "givenName;lang-en":
						assertEquals("givenName", m.getAttribute().getBaseName());
						assertEquals("Mary", m.getValues()[0]);
						break;
					default:
						fail();
				}
			}

			if (ModificationType.ADD.equals(m.getModificationType())) {

				switch (m.getAttribute().getName()) {

					case "givenName;lang-en":
						assertEquals("givenName", m.getAttribute().getBaseName());
						assertEquals("Alice", m.getValues()[0]);
						break;
					case "points":
						assertEquals("1000", m.getValues()[0]);
						break;
					default:
						fail();
				}
			}
		}

		assertEquals(5, mods.size());
	}


	public void testNoDiff()
		throws LDAPException {

		// Compose identical directory entries
		Entry e1 = composeEntry();
		Entry e2 = composeEntry();

		LDAPModifyRequestFactory factory = new LDAPModifyRequestFactory(getModifiableAttributes());

		assertNull(factory.composeModifyRequest(e1, e2));
	}
	
	
	public void testAllowEmptyModifiableAttributes() {
		
		new LDAPModifyRequestFactory(Collections.emptySet());
	}
}
