<html>
<head>
    <title>Infinispan LDAP Cache Store</title>
</head>

<body>

    <p>Cache loader / writer for an LDAP directory backend.</p>

    <h2 id="requirements">Requirements</h2>

    <ul>
        <li>Infinispan 9+</li>
        <li>Java 8+</li>
        <li>LDAP v3 compatible directory server, such as OpenLDAP, 389DS,
            OpenDJ, MS Active Directory, etc.</li>
    </ul>

    <h2 id="features">Features</h2>

    <ul>
        <li>Implements the complete <code>AdvancedLoadWriteStore</code>
            SPI</li>
        <li>Provides an interface for transforming Infinispan entries to / from
            LDAP entries, with support for LDAP attribute options (such as
            language tags) and choosing an optimal write strategy</li>
        <li>Provides an optional interface for executing arbitrary LDAP
            searches against the directory, bypassing the standard Infinispan
            load store API</li>
        <li>Configurable LDAP connection pool which can utilise multiple LDAP
            servers in fail-over or round-robin selection mode</li>
        <li>Optional custom trust and key store for TLS connections to the LDAP
            server</li>
        <li>Detailed Dropwizard metrics for all LDAP operations</li>
        <li>System property interpolation for all configuration properties
            using a <code>${sys-prop-name:default-value}</code> format </li>
        <li>Multi-level logging via Log4j2</li>
        <li>Open source (Apache 2.0 license)</li>
    </ul>

    <h2 id="usage">Usage</h2>

    <ol>
        <li>Add the Maven dependency for the LDAP cache store, or make sure its
            JAR is present in the CLASSPATH of your project.</li>
        <li>Implement <code>LDAPEntryTransformer</code> to translate between
            Infinispan entries (key / value pairs with optional metadata) and
            LDAP directory entries (identified by a DN and consisting of
            attributes).</li>
        <li>Set up a user account for the authenticated LDAP connections to the
            directory. The connections will be authenticated by a simple bind
            to the account's DN. The LDAP cache store client can also work with
            unauthenticated connections.</li>
        <li>Create an LDAP directory branch (typically an
            <code>organizationalUnit</code> <code>objectClass</code>) where the
            persisted LDAP entries are located or going to be written. The LDAP
            user account must have the appropriate permissions to access this
            branch as well as any immediate entries under it.</li>
        <li>Configure an LDAP store for each Infinispan cache that requires
            one, by setting the properties specified in
            <code>LDAPStoreConfiguration</code>. Also, see the example
            below. Note that the LDAP store can safely shared between multiple
            replicated / distributed instances of a cache. It can also be used
            in read-only mode.</li>
    </ol>

    <h2 id="maven">Maven</h2>

    <p>Maven coordinates:</p>

<pre>
    &lt;groupId&gt;com.nimbusds&lt;/groupId&gt;
    &lt;artifactId&gt;infinispan-cachestore-ldap&lt;/artifactId&gt;
    &lt;version&gt;[ version ]&lt;/version&gt;
</pre>

    <p>where <code>[ version ]</code> should be the latest stable version.</p>

    <h2 id="example-config">Example configuration</h2>

    <p>Example Infinispan configuration for a cache backed by an LDAP directory:</p>

<pre>
&lt;infinispan xmlns:xsi=&quot;http://www.w3.org/2001/XMLSchema-instance&quot;
            xsi:schemaLocation=&quot;urn:infinispan:config:9.2 http://www.infinispan.org/schemas/infinispan-config-9.2.xsd&quot;
            xmlns=&quot;urn:infinispan:config:9.2&quot;&gt;

    &lt;cache-container name=&quot;myCacheContainer&quot; default-cache=&quot;myMap&quot; statistics=&quot;true&quot;&gt;
        &lt;jmx duplicate-domains=&quot;true&quot;/&gt;
        &lt;local-cache name=&quot;myMap&quot;&gt;
            &lt;eviction type=&quot;COUNT&quot; size=&quot;100&quot;/&gt;
            &lt;persistence passivation=&quot;false&quot;&gt;
                &lt;store class=&quot;com.nimbusds.infinispan.persistence.ldap.LDAPStore&quot;
                        fetch-state=&quot;false&quot;
                        preload=&quot;false&quot;
                        shared=&quot;true&quot;
                        purge=&quot;false&quot;
                        read-only=&quot;false&quot;
                        singleton=&quot;false&quot;&gt;

                    &lt;!-- LDAP user details --&gt;
                    &lt;property name=&quot;ldapUser.dn&quot;&gt;cn=Directory Manager&lt;/property&gt;
                    &lt;property name=&quot;ldapUser.password&quot;&gt;secret&lt;/property&gt;

                    &lt;!-- LDAP server details --&gt;
                    &lt;property name=&quot;ldapServer.url&quot;&gt;ldap://127.0.0.1:30389&lt;/property&gt;
                    &lt;property name=&quot;ldapServer.selectionAlgorithm&quot;&gt;FAILOVER&lt;/property&gt;
                    &lt;property name=&quot;ldapServer.connectTimeout&quot;&gt;500&lt;/property&gt;
                    &lt;property name=&quot;ldapServer.responseTimeout&quot;&gt;500&lt;/property&gt;
                    &lt;property name=&quot;ldapServer.security&quot;&gt;none&lt;/property&gt;
                    &lt;property name=&quot;ldapServer.trustSelfSignedCerts&quot;&gt;false&lt;/property&gt;
                    &lt;property name=&quot;ldapServer.connectionPoolSize&quot;&gt;10&lt;/property&gt;
                    &lt;property name=&quot;ldapServer.connectionPoolInitialSize&quot;&gt;0&lt;/property&gt;
                    &lt;property name=&quot;ldapServer.connectionPoolMaxWaitTime&quot;&gt;250&lt;/property&gt;
                    &lt;property name=&quot;ldapServer.connectionMaxAge&quot;&gt;0&lt;/property&gt;

                    &lt;!-- LDAP directory entry details --&gt;
                    &lt;property name=&quot;ldapDirectory.baseDN&quot;&gt;ou=people, dc=wonderland, dc=net&lt;/property&gt;
                    &lt;property name=&quot;ldapDirectory.pageSize&quot;&gt;500&lt;/property&gt;
                    &lt;property name=&quot;ldapDirectory.entryTransformer&quot;&gt;com.nimbusds.infinispan.persistence.ldap.UserEntryTransformer&lt;/property&gt;

                    &lt;!-- Custom LDAP sever trust and key store --&gt;
                    &lt;property name=&quot;customTrustStore.enable&quot;&gt;false&lt;/property&gt;
                    &lt;property name=&quot;customTrustStore.file&quot;&gt;keystore.jks&lt;/property&gt;
                    &lt;property name=&quot;customTrustStore.password&quot;&gt;secret&lt;/property&gt;
                    &lt;property name=&quot;customTrustStore.type&quot;&gt;JKS&lt;/property&gt;

                    &lt;property name=&quot;customKeyStore.enable&quot;&gt;false&lt;/property&gt;
                    &lt;property name=&quot;customKeyStore.file&quot;&gt;keystore.jks&lt;/property&gt;
                    &lt;property name=&quot;customKeyStore.password&quot;&gt;secret&lt;/property&gt;
                    &lt;property name=&quot;customKeyStore.type&quot;&gt;JKS&lt;/property&gt;
                &lt;/store&gt;
            &lt;/persistence&gt;
        &lt;/local-cache&gt;
    &lt;/cache-container&gt;

&lt;/infinispan&gt;
</pre>


</body>

</html>
