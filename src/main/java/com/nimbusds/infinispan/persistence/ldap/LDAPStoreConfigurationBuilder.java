package com.nimbusds.infinispan.persistence.ldap;


import java.util.Properties;

import org.infinispan.configuration.cache.AbstractStoreConfiguration;
import org.infinispan.configuration.cache.AbstractStoreConfigurationBuilder;
import org.infinispan.configuration.cache.PersistenceConfigurationBuilder;


/**
 * LDAP store configuration builder.
 *
 * <p>Used by the Infinispan ConfigurationBuilder to implement fluent
 * configuration for the LDAP CacheLoader / CacheWriter. Methods should use
 * the fluent style, rather than the setter/getter style and should return an
 * instance of this object.
 */
public class LDAPStoreConfigurationBuilder extends AbstractStoreConfigurationBuilder<LDAPStoreConfiguration,LDAPStoreConfigurationBuilder> {


	public LDAPStoreConfigurationBuilder(final PersistenceConfigurationBuilder builder) {
		super(builder, AbstractStoreConfiguration.attributeDefinitionSet());
	}


	@Override
	public LDAPStoreConfiguration create() {
        	// This method should construct a new instance of a
		// LDAPStoreConfiguration object. There will be one instance
		// per cache.
		return new LDAPStoreConfiguration(
			purgeOnStartup,
			fetchPersistentState,
			ignoreModifications,
			async.create(),
			singletonStore.create(),
			preload,
			shared,
			properties);
	}


	@Override
	public LDAPStoreConfigurationBuilder withProperties(final Properties properties) {
		return properties(properties);
	}


	@Override
	public LDAPStoreConfigurationBuilder self() {
		return this;
	}
}
