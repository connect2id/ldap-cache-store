package com.nimbusds.infinispan.persistence.ldap.query;


import com.nimbusds.infinispan.persistence.ldap.LDAPEntryTransformer;
import com.nimbusds.infinispan.persistence.ldap.backend.LDAPConnector;


/**
 * LDAP search query executor initialisation context.
 */
public interface LDAPQueryExecutorInitContext<K,V> {
	
	
	/**
	 * Returns the configured LDAP connector.
	 *
	 * @return The LDAP connector.
	 */
	LDAPConnector getLDAPConnector();
	
	
	/**
	 * Returns the configured LDAP entry transformer.
	 *
	 * @return The LDAP entry transformer.
	 */
	LDAPEntryTransformer<K,V> getLDAPEntryTransformer();
}
