package com.nimbusds.infinispan.persistence.ldap.query;


import com.nimbusds.infinispan.persistence.common.query.QueryExecutor;


/**
 * LDAP search query executor.
 */
public interface LDAPQueryExecutor<K,V> extends QueryExecutor<K,V> {
	
	
	/**
	 * Initialises the LDAP search query executor.
	 *
	 * @param initCtx The initialisation context. Not {@code null}.
	 */
	void init(final LDAPQueryExecutorInitContext<K,V> initCtx);
}
