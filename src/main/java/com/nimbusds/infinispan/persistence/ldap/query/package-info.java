/**
 * LDAP search query executor interfaces.
 */
package com.nimbusds.infinispan.persistence.ldap.query;