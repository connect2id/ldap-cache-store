/**
 * LDAP backend related classes.
 */
package com.nimbusds.infinispan.persistence.ldap.backend;