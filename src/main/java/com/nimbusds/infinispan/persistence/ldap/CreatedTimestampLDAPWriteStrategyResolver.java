package com.nimbusds.infinispan.persistence.ldap;


import java.util.Date;

import com.nimbusds.infinispan.persistence.common.InfinispanEntry;
import net.jcip.annotations.Immutable;


/**
 * LDAP write strategy resolver based on the created timestamp found in the
 * metadata of Infinispan entries. Recently created Infinispan entries are
 * given {@link LDAPWriteStrategy#TRY_LDAP_ADD_FIRST}, else
 * {@link LDAPWriteStrategy#TRY_LDAP_MODIFY_FIRST}. Note that entries without
 * expiration are not assigned a created timestamp (-1) in which case the
 * resolver returns {@link LDAPWriteStrategy#TRY_LDAP_ADD_FIRST} (the default
 * strategy).
 */
@Immutable
public class CreatedTimestampLDAPWriteStrategyResolver<K,V> implements LDAPWriteStrategyResolver<K,V> {


	/**
	 * The default age (50 milliseconds) of entries considered recent.
	 */
	public static final long DEFAULT_RECENT_AGE_MS = 50;


	/**
	 * The age (in milliseconds) of entries considered recent.
	 */
	private final long recentAgeMs;


	/**
	 * Creates a new LDAP write strategy resolver based on the created
	 * timestamp of Infinispan entries where entries aged younger than
	 * 50 milliseconds are given
	 * {@link LDAPWriteStrategy#TRY_LDAP_ADD_FIRST}.
	 */
	public CreatedTimestampLDAPWriteStrategyResolver() {
		this(DEFAULT_RECENT_AGE_MS);
	}


	/**
	 * Creates a new LDAP write strategy resolver based on the created
	 * timestamp of Infinispan entries where entries aged younger than
	 * the specified age are given
	 * {@link LDAPWriteStrategy#TRY_LDAP_ADD_FIRST}.
	 *
	 * @param recentAgeMs The entry age (in milliseconds) that is
	 *                    considered recent. Must be non-negative.
	 */
	public CreatedTimestampLDAPWriteStrategyResolver(long recentAgeMs) {
		assert recentAgeMs >= 0;
		this.recentAgeMs = recentAgeMs;
	}
	

	@Override
	public LDAPWriteStrategy resolveLDAPWriteStrategy(final InfinispanEntry<K,V> infinispanEntry) {

		if (infinispanEntry.getMetadata() == null) {
			return LDAPWriteStrategy.getDefault();
		}

		if (infinispanEntry.getMetadata().created() < 0L) {
			return LDAPWriteStrategy.getDefault();
		}

		final Date now = new Date();

		if (infinispanEntry.getMetadata().created() + recentAgeMs > now.getTime()) {
			// New entry
			return LDAPWriteStrategy.TRY_LDAP_ADD_FIRST;
		} else {
			// Entry which has been around for some time
			return LDAPWriteStrategy.TRY_LDAP_MODIFY_FIRST;
		}
	}
}
