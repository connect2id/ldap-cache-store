package com.nimbusds.infinispan.persistence.ldap.backend;


import java.util.*;

import com.unboundid.ldap.sdk.*;
import net.jcip.annotations.ThreadSafe;


/**
 * LDAP modify request factory. Supports attribute options (such as language
 * tags).
 */
@ThreadSafe
class LDAPModifyRequestFactory {


	/**
	 * The names of the modifiable directory attributes.
	 */
	private final Set<String> modifiableAttributes;


	/**
	 * Creates a new LDAP request factory.
	 *
	 * @param attributes The names of the modifiable directory attributes.
	 *                   Must not include {@code objectClass}, attributes
	 *                   that form the RDN, or operational attributes.
	 *                   Empty if modification is not permitted or
	 *                   supported. Must not be {@code null}.
	 */
	public LDAPModifyRequestFactory(final Set<String> attributes) {
		assert ! attributes.contains("objectClass");
		this.modifiableAttributes = attributes;
	}


	/**
	 * Returns the names of the modifiable directory attributes.
	 *
	 * @return The names of the modifiable directory attributes.
	 */
	public Set<String> getModifiableAttributes() {
		return modifiableAttributes;
	}


	/**
	 * Composes an LDAP modify request for the specified entry.
	 *
	 * @param entry The entry. Must not be {@code null}.
	 *
	 * @return The modify request.
	 */
	public ModifyRequest composeModifyRequest(final Entry entry) {

		List<Modification> mods = new LinkedList<>();

		// Compose list of attribute updates
		List<String> updatedAttributes = new LinkedList<>();

		for (com.unboundid.ldap.sdk.Attribute attr: entry.getAttributes()) {

			if (! getModifiableAttributes().contains(attr.getBaseName())) {
				continue; // skip, attribute not modifiable
			}

			// Flag attribute for replace, set new value(s)
			mods.add(new Modification(ModificationType.REPLACE, attr.getName(), attr.getValues()));
			updatedAttributes.add(attr.getBaseName()); // trim any lang-tags
		}

		// Compose list of attribute deletions
		for (String supAttr: getModifiableAttributes()) {

			if (! updatedAttributes.contains(supAttr)) {
				// Flag for delete via replace
				mods.add(new Modification(ModificationType.REPLACE, supAttr));
			}
		}

		return new ModifyRequest(entry.getDN(), mods);
	}


	/**
	 * Composes an LDAP modify request for the specified entry.
	 *
	 * @param newEntry The new entry. Must not be {@code null}.
	 * @param oldEntry The old entry. Must not be {@code null}.
	 *
	 * @return The modify request, {@code null} if no modification is
	 *         required (no diff between new and old entry).
	 */
	public ModifyRequest composeModifyRequest(final Entry newEntry, final Entry oldEntry) {

		String[] attrs = modifiableAttributes.toArray(new String[modifiableAttributes.size()]);

		List<Modification> mods = Entry.diff(oldEntry, newEntry, true, attrs);

		if (mods.isEmpty()) {
			// No diff
			return null;
		}

		return new ModifyRequest(newEntry.getDN(), mods);
	}
}
