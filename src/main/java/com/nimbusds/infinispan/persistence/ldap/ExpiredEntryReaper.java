package com.nimbusds.infinispan.persistence.ldap;


import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import com.nimbusds.infinispan.persistence.common.InfinispanEntry;
import com.nimbusds.infinispan.persistence.ldap.backend.LDAPConnector;
import com.unboundid.ldap.sdk.DN;
import com.unboundid.ldap.sdk.LDAPException;
import net.jcip.annotations.ThreadSafe;
import org.apache.commons.lang3.tuple.Pair;
import org.infinispan.marshall.core.MarshalledEntry;
import org.infinispan.marshall.core.MarshalledEntryFactory;
import org.infinispan.metadata.InternalMetadata;
import org.infinispan.persistence.spi.AdvancedCacheExpirationWriter;
import org.infinispan.persistence.spi.AdvancedCacheWriter;
import org.infinispan.persistence.spi.PersistenceException;


/**
 * Expired entry reaper.
 */
@ThreadSafe
class ExpiredEntryReaper<K,V> {
	
	
	/**
	 * The Infinispan marshalled entry factory.
	 */
	private final MarshalledEntryFactory<K,V> marshalledEntryFactory;


	/**
	 * The LDAP connector.
	 */
	private final LDAPConnector ldapConnector;


	/**
	 * The LDAP entity transformer.
	 */
	private final LDAPEntryTransformer<K,V> entityTransformer;


	/**
	 * Creates a new reaper for expired entries.
	 *
	 * @param marshalledEntryFactory The Infinispan marshalled entry
	 *                               factory. Must not be {@code null}.
	 * @param ldapConnector          The LDAP connector. Must not be
	 *                               {@code null}.
	 * @param entityTransformer      The LDAP entity transformer. Must not
	 *                               be {@code null}.
	 */
	public ExpiredEntryReaper(final MarshalledEntryFactory<K,V> marshalledEntryFactory,
				  final LDAPConnector ldapConnector,
				  final LDAPEntryTransformer<K,V> entityTransformer) {

		assert marshalledEntryFactory != null;
		this.marshalledEntryFactory = marshalledEntryFactory;
		
		assert ldapConnector != null;
		this.ldapConnector = ldapConnector;

		assert entityTransformer != null;
		this.entityTransformer = entityTransformer;
	}


	/**
	 * Purges the expired persisted entries according to their metadata
	 * timestamps (if set / persisted).
	 *
	 * @param purgeListener The purge listener. Must not be {@code null}.
	 *
	 * @return The number of purged entries.
	 */
	public int purge(final AdvancedCacheWriter.PurgeListener<? super K> purgeListener) {

		final long now = new Date().getTime();

		// Marks DN + key of entries for deletion
		List<Pair<DN,K>> forDeletion = new LinkedList<>();

		ldapConnector.retrieveEntries(ldapEntry -> {

			InfinispanEntry<K,V> infinispanEntry = entityTransformer.toInfinispanEntry(new LDAPEntry(ldapEntry));
			
			InternalMetadata metadata = infinispanEntry.getMetadata();
			
			if (metadata == null) {
				return; // no metadata found
			}
			
			if (metadata.isExpired(now)) {

				// Mark entry for deletion

				try {
					Pair<DN,K> pair = Pair.of(
						new DN(ldapEntry.getDN()),
						infinispanEntry.getKey());

					forDeletion.add(pair);

				} catch (LDAPException e) {
					throw new PersistenceException(e.getMessage(), e);
				}
			}
		});


		int deleteCounter = 0;

		for (Pair<DN,K> pair: forDeletion) {

			// Delete LDAP entry by its DN
			if (ldapConnector.deleteEntry(pair.getLeft())) {

				// Notify listener, interested in the Infinispan entry key
				purgeListener.entryPurged(pair.getRight());

				deleteCounter++;
			}
		}
		
		Loggers.LDAP_LOG.debug("[IL0255] LDAP store: Purged {} expired entries", deleteCounter);

		return deleteCounter;
	}


	/**
	 * Purges the expired persisted entries according to their metadata
	 * timestamps (if set / persisted), with an extended listener for the
	 * complete purged entry.
	 *
	 * @param purgeListener The purge listener. Must not be {@code null}.
	 *
	 * @return The number of purged entries.
	 */
	public int purgeExtended(AdvancedCacheExpirationWriter.ExpirationPurgeListener<K,V> purgeListener) {

		final long now = new Date().getTime();

		// The entries for deletion
		List<Pair<DN,InfinispanEntry<K,V>>> forDeletion = new LinkedList<>();

		ldapConnector.retrieveEntries(ldapEntry -> {

			InfinispanEntry<K,V> infinispanEntry = entityTransformer.toInfinispanEntry(new LDAPEntry(ldapEntry));
			
			InternalMetadata metadata = infinispanEntry.getMetadata();
			
			if (metadata == null) {
				return; // no metadata found
			}
			
			if (metadata.isExpired(now)) {

				// Mark entry for deletion
				try {
					Pair<DN,InfinispanEntry<K,V>> pair = Pair.of(
						new DN(ldapEntry.getDN()),
						infinispanEntry);

					forDeletion.add(pair);

				} catch (LDAPException e) {
					throw new PersistenceException(e.getMessage(), e);
				}
			}
		});


		int deleteCounter = 0;

		for (Pair<DN,InfinispanEntry<K,V>> pair: forDeletion) {

			// Delete LDAP entry by its DN
			if (ldapConnector.deleteEntry(pair.getLeft())) {
				
				// Notify listener, interested in the Infinispan entry
				InfinispanEntry<K,V> en = pair.getRight();
				
				MarshalledEntry<K,V> marshalledEntry =
					marshalledEntryFactory.newMarshalledEntry(
						en.getKey(),
						en.getValue(),
						en.getMetadata()
					);
				purgeListener.marshalledEntryPurged(marshalledEntry);
				deleteCounter++;
			}
		}
		
		Loggers.LDAP_LOG.debug("[IL0255] LDAP store: Purged {} expired entries", deleteCounter);

		return deleteCounter;
	}
}
