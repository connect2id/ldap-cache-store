package com.nimbusds.infinispan.persistence.ldap;


import java.util.Properties;

import com.nimbusds.common.config.*;
import com.thetransactioncompany.util.PropertyParseException;
import com.thetransactioncompany.util.PropertyRetriever;
import com.unboundid.ldap.sdk.DN;
import com.unboundid.ldap.sdk.LDAPException;
import net.jcip.annotations.Immutable;
import org.apache.commons.collections4.MapUtils;
import org.infinispan.commons.configuration.BuiltBy;
import org.infinispan.commons.configuration.ConfigurationFor;
import org.infinispan.commons.util.StringPropertyReplacer;
import org.infinispan.configuration.cache.AbstractStoreConfiguration;
import org.infinispan.configuration.cache.AsyncStoreConfiguration;
import org.infinispan.configuration.cache.SingletonStoreConfiguration;


/**
 * LDAP store configuration. It's typically derived from a Java key / value
 * properties file. The configuration is stored as public fields which become
 * immutable (final) after their initialisation.
 *
 * <p>Example LDAP store configuration:
 *
 * <pre>
 * # LDAP server details #
 * ldapServer.url = ldap://localhost:1389 ldap://remotehost:1389
 * ldapServer.selectionAlgorithm = FAILOVER
 * ldapServer.connectTimeout = 100
 * ldapServer.responseTimeout = 100
 * ldapServer.security = NONE
 * ldapServer.trustSelfSignedCerts = false
 * ldapServer.connectionPoolSize = 5
 * ldapServer.connectionPoolInitialSize = 0
 * ldapServer.connectionPoolMaxWaitTime = 100
 * ldapServer.connectionMaxAge = 0
 *
 * # LDAP user details #
 * ldapUser.dn = cn=Directory Manager
 * ldapUser.password = secret
 *
 * # LDAP directory entry details #
 * ldapDirectory.baseDN = ou=authorizations, dc=wonderland, dc=net
 * ldapDirectory.pageSize = 500
 * ldapDirectory.entryTransformer = com.nimbusds.infinispan.persistence.ldap.UserEntityTransformer
 * ldapDirectory.queryExecutor = com.nimbusds.infinispan.persistence.ldap.UserQueryExecutor
 *
 * # Custom LDAP sever trust and key store #
 * customTrustStore.enable = false
 * customTrustStore.file =
 * customTrustStore.password =
 * customTrustStore.type =
 *
 * customKeyStore.enable = false
 * customKeyStore.file =
 * customKeyStore.password =
 * customKeyStore.type =
 * </pre>
 */
@Immutable
@BuiltBy(LDAPStoreConfigurationBuilder.class)
@ConfigurationFor(LDAPStore.class)
public class LDAPStoreConfiguration extends AbstractStoreConfiguration implements LoggableConfiguration {


	/**
	 * LDAP directory entry configuration.
	 */
	public static class LDAPDirectory implements LoggableConfiguration {


		/**
		 * The distinguished name (DN) of the base directory entry
		 * under which the persisted entries are stored.
		 *
		 * <p>Property key: ldapDirectory.baseDN
		 */
		public final DN baseDN;


		/**
		 * The page size to use when retrieving multiple directory
		 * entries.
		 *
		 * <p>See LDAP Control Extension for Simple Paged Results
		 * Manipulation (RFC 2696).
		 *
		 * <p>Property key: ldapDirectory.pageSize
		 */
		public final int pageSize;



		/**
		 * The name of the class for transforming between Infinispan
		 * entries (key / value pair and optional metadata) and a
		 * corresponding LDAP entry (DN with attributes).
		 *
		 * <p>See {@link LDAPEntryTransformer}.
		 *
		 * <p>Property key: ldapDirectory.entryTransformer
		 */
		public final String entryTransformer;
		
		
		/**
		 * The name of the optional class for executing direct LDAP
		 * searches against the directory server.
		 *
		 * <p>See {@link com.nimbusds.infinispan.persistence.common.query.QueryExecutor}
		 *
		 * <p>Property key: ldapDirectory.queryExecutor.
		 */
		public final String queryExecutor;


		/**
		 * Creates a new LDAP directory entry configuration from the
		 * specified properties.
		 *
		 * @param props The properties. Must not be {@code null}.
		 *
		 * @throws PropertyParseException On a missing or invalid
		 *                                property.
		 */
		public LDAPDirectory(final Properties props)
			throws PropertyParseException {

			PropertyRetriever pr = new PropertyRetriever(props);

			String dnString = null;
			try {
				dnString = pr.getString("ldapDirectory.baseDN");
				baseDN = new DN(dnString);
			} catch (LDAPException e) {
				throw new PropertyParseException("Invalid DN", "ldapDirectory.baseDN", dnString);
			}

			pageSize = pr.getInt("ldapDirectory.pageSize");

			if (pageSize < 0) {
				throw new PropertyParseException("The page size must be 0 (paging disabled) or positive", "ldapDirectory.pageSize", pageSize + "");
			}

			entryTransformer = pr.getString("ldapDirectory.entryTransformer");
			
			queryExecutor = pr.getOptString("ldapDirectory.queryExecutor", null);
		}


		@Override
		public void log() {

			Loggers.MAIN_LOG.info("[IL0000] Infinispan LDAP store: Directory base DN: {} ", baseDN);
			Loggers.MAIN_LOG.info("[IL0001] Infinispan LDAP store: Page size: {} ", pageSize);
			Loggers.MAIN_LOG.info("[IL0002] Infinispan LDAP store: Entry transformer class: {} ", entryTransformer);
			Loggers.MAIN_LOG.info("[IL0003] Infinispan LDAP store: Query executor class: {} ", queryExecutor != null ? queryExecutor : "not specified");
		}
	}


	/**
	 * The LDAP server details.
	 *
	 * <p>Property key: ldapServer
	 */
	public final LDAPServerConnectionPoolDetails ldapServer;


	/**
	 * The LDAP directory user details. The user must have read and add
	 * permissions to the directory tree containing the persisted entries.
	 *
	 * <p>Property key: ldapUser
	 */
	public final DirectoryUser ldapUser;


	/**
	 * The LDAP directory entry details.
	 *
	 * <p>Property key: ldapDirectory
	 */
	public final LDAPDirectory ldapDirectory;


	/**
	 * The custom trust store for secure LDAP server connections.
	 *
	 * <p>Property key: customTrustStore
	 */
	public final CustomTrustStoreConfiguration customTrustStore;


	/**
	 * The custom key store for secure LDAP server connections.
	 *
	 * <p>Property key: customKeyStore
	 */
	public final CustomKeyStoreConfiguration customKeyStore;


	/**
	 * Creates a new LDAP store configuration from the specified
	 * properties. All other settings assume defaults.
	 *
	 * @param properties The LDAP store specific configuration properties.
	 *                   Must not be {@code null}.
	 */
	public LDAPStoreConfiguration(final Properties properties) {

		this(
			false, // purgeOnStartup
			false, // fetchPersistentState
			false, // ignoreModifications
			null,  // AsyncStoreConfiguration
			null,  // SingletonStoreConfiguration
			false, // preload
			true, // shared
			properties);
	}


	/**
	 * Creates a new LDAP store configuration.
	 *
	 * @param purgeOnStartup       If {@code true} the cache store will be
	 *                             purged when it starts up.
	 * @param fetchPersistentState If {@code true} the persistent state
	 *                             be fetched when joining a cluster.
	 * @param ignoreModifications  If {@code true} any operation that
	 *                             modifies the cache (put, remove, clear,
	 *                             store...etc) won't be applied to the
	 *                             cache store. This means that the cache
	 *                             store could become out of sync with the
	 *                             cache.
	 * @param async                Configuration for the async cache
	 *                             loader.
	 * @param singletonStore       Configuration for a singleton store.
	 * @param preload              If {@code true} when the cache starts
	 *                             data stored in the cache loader will be
	 *                             pre-loaded into memory.
	 * @param shared               If {@code true} the cache store is
	 *                             shared among all cache instances.
	 * @param properties           The LDAP store specific configuration
	 *                             properties. Must not be {@code null}.
	 */
	public LDAPStoreConfiguration(final boolean purgeOnStartup,
				      final boolean fetchPersistentState,
				      final boolean ignoreModifications,
				      final AsyncStoreConfiguration async,
				      final SingletonStoreConfiguration singletonStore,
				      final boolean preload,
				      final boolean shared,
				      final Properties properties) {

		super(
			purgeOnStartup,
			fetchPersistentState,
			ignoreModifications,
			async,
			singletonStore,
			preload,
			shared,
			properties);

		if (MapUtils.isEmpty(properties)) {
			throw new ConfigurationException("Missing LDAP store configuration properties, check the service documentation");
		}
		
		// Interpolate with system properties where ${sysPropName} is found
		Properties interpolatedProps = new Properties();
		for (String name: properties.stringPropertyNames()) {
			interpolatedProps.setProperty(name, StringPropertyReplacer.replaceProperties(properties.getProperty(name)));
		}
		
		try {
			ldapServer = new LDAPServerConnectionPoolDetails("ldapServer.", interpolatedProps);
			ldapUser = new DirectoryUser("ldapUser.", interpolatedProps);
			ldapDirectory = new LDAPDirectory(interpolatedProps);
			customTrustStore = new CustomTrustStoreConfiguration("customTrustStore.", interpolatedProps);
			customKeyStore = new CustomKeyStoreConfiguration("customKeyStore.", interpolatedProps);

		} catch (PropertyParseException e) {
			throw new ConfigurationException(e.getMessage() +
				": Property: " + e.getPropertyKey() +
				": Value: " + e.getPropertyValue());
		}
	}


	@Override
	public void log() {
		ldapServer.log();
		ldapUser.log();
		ldapDirectory.log();
		customTrustStore.log();
		customKeyStore.log();
	}
}
