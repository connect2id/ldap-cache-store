package com.nimbusds.infinispan.persistence.ldap;


import java.util.Set;

import com.nimbusds.infinispan.persistence.common.InfinispanEntry;
import com.unboundid.ldap.sdk.DN;
import com.unboundid.ldap.sdk.RDN;


/**
 * Interface for transforming between Infinispan entries (key / value pair and
 * metadata) and a corresponding LDAP entry (consisting of a Distinguished Name
 * (DN) and attributes). Implementations must be thread-safe.
 *
 * <p>To specify an entry transformer for a given Infinispan cache that is
 * backed by an LDAP store, provide its class name to the
 * {@link com.nimbusds.infinispan.persistence.ldap.LDAPStoreConfiguration
 * store configuration}.
 */
public interface LDAPEntryTransformer<K,V> {


	/**
	 * Returns the names of the LDAP directory attributes that may be
	 * modified when an Infinispan entry is updated or replaced via the
	 * {@link java.util.Map} interface. Attributes that identify the LDAP
	 * entry (form its Relative Distinguished Name (RDN)) and the
	 * attribute {@code objectClass} that defined its classes are typically
	 * immutable for an entry and therefore must not be included.
	 * Operational (meta) attributes must not be included either.
	 *
	 * <p>Example:
	 *
	 * <ul>
	 *     <li>Java object {@code User} with fields {@code id},
	 *     {@code givenName}, {@code surname} and {@code email}.
	 *     <li>These fields map to the LDAP attributes {@code uid},
	 *     {@code givenName}, {@code sn} and {@code mail} of the LDAP class
	 *     {@code inetOrgPerson}.
	 *     <li>Of of the above LDAP attributes, {@code uid} is never
	 *     changed for a user, and only {@code givenName}, {@code sn} and
	 *     {@code mail} may be updated. This method should then return the
	 *     set {@code givenName, sn, mail}.
	 * </ul>
	 *
	 * @return The names of the modifiable directory attributes. Must not
	 *         include {@code objectClass}, attributes that form the
	 *         Relative Distinguished Name (RDN), or operational
	 *         attributes. Must not be empty or {@code null}.
	 */
	Set<String> getModifiableAttributes();


	/**
	 * Returns {@code true} if any of the {@link #getModifiableAttributes
	 * modifiable attributes} may include options (such as a flag
	 * indicating a binary value, or a language tag).
	 *
	 * @return {@code true} if any of the modifiable attributes may
	 *         include options (such as a binary flag or a language tag),
	 *         else {@code false}.
	 */
	boolean includesAttributesWithOptions();


	/**
	 * Resolves the specified Infinispan entry key to a Relative
	 * Distinguished Name (RDN) for the matching LDAP directory entry.
	 *
	 * <p>Example:
	 *
	 * <ul>
	 *     <li>Java object {@code User} keyed by its {@code id} field which
	 *     is a {@link java.lang.String}.
	 *     <li>The Java {@code id} fields maps to the LDAP attribute
	 *     {@code uid}.
	 *     <li>The {@code "cae7t"} key must then resolve to the RDN
	 *     {@code uid=cae7t}.
	 * </ul>
	 *
	 * <p>Composite keys may map to RDNs with multiple attributes, e.g.
	 * {@code uid=cae7t+ou=sales}.
	 *
	 * @param key The key. Not {@code null}.
	 *
	 * @return The resolved RDN.
	 */
	RDN resolveRDN(final K key);
	

	/**
	 * Transforms the specified Infinispan entry (key / value pair with
	 * optional metadata) to an LDAP directory entry ready to be written.
	 * The LDAP entry may be supplied with an {@link LDAPWriteStrategy}.
	 *
	 * <p>Example:
	 *
	 * <ul>
	 *     <li>Base DN: {@code ou=people,dc=wondlerland,dc=net}
	 *     <li>Key: cae7t
	 *     <li>Value: Java POJO with fields {@code uid=cae7t},
	 *         {@code givenName=Alice}, {@code surname=Adams} and
	 *         {@code email=alice@wonderland.net}.
	 *     <li>Metadata: Specifies the entry expiration and other
	 *         properties.
	 * </ul>
	 *
	 * <p>Resulting LDAP entry:</p>
	 *
	 * <pre>
	 * dn: uid=cae7t,ou=people,dc=wondlerland,dc=net
	 * objectClass: top
	 * objectClass: person
	 * objectClass: organizationalPerson
	 * objectClass: inetOrgPerson
	 * uid: cae7t
	 * cn: Alice Adams
	 * sn: Adams
	 * givenName: Alice
	 * mail: alice@wonderland.net
	 * </pre>
	 *
	 * @param baseDN          The Distinguished Name (DN) of the directory
	 *                        branch where the entry is located. Not
	 *                        {@code null}.
	 * @param infinispanEntry The Infinispan entry. Not {@code null}.
	 *
	 * @return The LDAP entry.
	 */
	LDAPEntry toLDAPEntry(final DN baseDN, final InfinispanEntry<K,V> infinispanEntry);


	/**
	 * Transforms the specified LDAP entry to an Infinispan entry (key /
	 * value / metadata triple).
	 *
	 * <p>Example:
	 *
	 * <p>LDAP entry:</p>
	 *
	 * <pre>
	 * dn: uid=cae7t,ou=people,dc=wondlerland,dc=net
	 * objectClass: top
	 * objectClass: person
	 * objectClass: organizationalPerson
	 * objectClass: inetOrgPerson
	 * uid: cae7t
	 * cn: Alice Adams
	 * sn: Adams
	 * givenName: Alice
	 * mail: alice@wonderland.net
	 * </pre>
	 *
	 * <p>Resulting key / value pair:
	 *
	 * <ul>
	 *     <li>Key: cae7t
	 *     <li>Value: Java POJO with fields {@code uid=cae7t},
	 *         {@code givenName=Alice}, {@code surname=Adams} and
	 *         {@code email=alice@wonderland.net}.
	 *     <li>Metadata: Default metadata (no expiration, etc).
	 * </ul>
	 *
	 * @param ldapEntry The LDAP entry. Not {@code null}.
	 *
	 * @return The Infinispan entry (key / value pair with optional
	 *         metadata).
	 */
	InfinispanEntry<K,V> toInfinispanEntry(final LDAPEntry ldapEntry);
}
