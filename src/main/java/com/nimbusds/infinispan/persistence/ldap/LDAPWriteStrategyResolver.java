package com.nimbusds.infinispan.persistence.ldap;


import com.nimbusds.infinispan.persistence.common.InfinispanEntry;


/**
 * Interface for resolving the appropriate LDAP write strategy for a given
 * Infinispan entry. Implementations must be thread-safe.
 */
public interface LDAPWriteStrategyResolver<K,V> {


	/**
	 * Resolves the appropriate LDAP write strategy for the specified
	 * Infinispan entry.
	 *
	 * @param infinispanEntry The Infinispan entry. Must not be
	 *                        {@code null}.
	 *
	 * @return The LDAP write strategy.
	 */
	LDAPWriteStrategy resolveLDAPWriteStrategy(final InfinispanEntry<K,V> infinispanEntry);
	
}
