package com.nimbusds.infinispan.persistence.ldap;


import com.unboundid.ldap.sdk.ReadOnlyEntry;
import net.jcip.annotations.Immutable;


/**
 * Encapsulates an LDAP entry with optional write strategy.
 */
@Immutable
public final class LDAPEntry {
	

	/**
	 * The LDAP entry consisting of DN and attributes.
	 */
	private final ReadOnlyEntry entry;


	/**
	 * Optional write strategy for the entry, {@code null} if not
	 * specified.
	 */
	private final LDAPWriteStrategy writeStrategy;


	/**
	 * Creates a new LDAP entry. No write strategy is specified.
	 *
	 * @param entry The LDAP entry consisting of DN and attributes. Must
	 *              not be {@code null}.
	 */
	public LDAPEntry(final ReadOnlyEntry entry) {

		this(entry, null);
	}


	/**
	 * Creates a new LDAP entry with optional write strategy.
	 *
	 * @param entry         The LDAP entry consisting of DN and
	 *                      attributes. Must not be {@code null}.
	 * @param writeStrategy Write strategy for the entry, {@code null} if
	 *                      not specified.
	 */
	public LDAPEntry(final ReadOnlyEntry entry, final LDAPWriteStrategy writeStrategy) {
		assert entry != null;
		this.entry = entry;
		this.writeStrategy = writeStrategy;
	}


	/**
	 * Returns the LDAP entry.
	 *
	 * @return The LDAP entry.
	 */
	public ReadOnlyEntry getEntry() {
		return entry;
	}


	/**
	 * Returns the write strategy for the entry.
	 *
	 * @return The write strategy, {@code null} if not specified.
	 */
	public LDAPWriteStrategy getWriteStrategy() {
		return writeStrategy;
	}
}
