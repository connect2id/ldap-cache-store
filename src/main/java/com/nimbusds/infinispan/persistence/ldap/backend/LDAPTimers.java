package com.nimbusds.infinispan.persistence.ldap.backend;


import com.codahale.metrics.Timer;
import com.nimbusds.common.monitor.MonitorRegistries;
import net.jcip.annotations.ThreadSafe;


/**
 * LDAP operation timers.
 */
@ThreadSafe
class LDAPTimers {


	/**
	 * Times LDAP get operations (search with scope=base).
	 */
	public final Timer getTimer = new Timer();


	/**
	 * Times LDAP search operations.
	 */
	public final Timer searchTimer = new Timer();


	/**
	 * Times LDAP add operations.
	 */
	public final Timer addTimer = new Timer();


	/**
	 * Times LDAP modify operations.
	 */
	public final Timer modifyTimer = new Timer();


	/**
	 * Times LDAP delete operations.
	 */
	public final Timer deleteTimer = new Timer();


	/**
	 * Creates a new set of LDAP operation timers.
	 *
	 * @param prefix The prefix for the timer names. Must not be
	 *               {@code null}.
	 */
	public LDAPTimers(final String prefix) {
		MonitorRegistries.register(prefix + "ldapStore.getTimer", getTimer);
		MonitorRegistries.register(prefix + "ldapStore.searchTimer", searchTimer);
		MonitorRegistries.register(prefix + "ldapStore.addTimer", addTimer);
		MonitorRegistries.register(prefix + "ldapStore.modifyTimer", modifyTimer);
		MonitorRegistries.register(prefix + "ldapStore.deleteTimer", deleteTimer);
	}
}