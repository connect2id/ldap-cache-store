package com.nimbusds.infinispan.persistence.ldap;


/**
 * Write strategy for persisting Infinispan entries into an LDAP directory.
 *
 * <p>Background:
 *
 * <p>LDAP directories don't support a generic write (or put) operation. New
 * entries are created with an ADD operation, and may then be updated with a
 * separate MODIFY operation. Generic write requests must therefore be
 * translated to an LDAP ADD or an LDAP MODIFY operation, and each operation
 * then tried individually until we have success.
 */
public enum LDAPWriteStrategy {


	/**
	 * Try to write the entry with an LDAP ADD operation first. If the ADD
	 * operation fails with an {@code 68} error code (entry already
	 * exists), then the entry write can then be attempted with an LDAP
	 * MODIFY operation.
	 */
	TRY_LDAP_ADD_FIRST,


	/**
	 * Try to write the entry with an LDAP MODIFY operation first. If the
	 * MODIFY operation fails with an {@code 32} (no such entry), then the
	 * entry write can be attempted with an LDAP ADD operation.
	 */
	TRY_LDAP_MODIFY_FIRST;


	/**
	 * Returns the default write strategy.
	 *
	 * @return The default write strategy ({@link #TRY_LDAP_ADD_FIRST}).
	 */
	public static LDAPWriteStrategy getDefault() {

		return TRY_LDAP_ADD_FIRST;
	}
}
