/**
 * LDAP store for Infinispan 8.2+ caches and maps.
 */
package com.nimbusds.infinispan.persistence.ldap;